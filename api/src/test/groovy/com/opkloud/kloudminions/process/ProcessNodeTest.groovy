package com.opkloud.kloudminions.process

import com.amazonaws.auth.AWSCredentials
import com.kloudminions.auth.AWSCredentialsFactory
import com.kloudminions.context.GlobalContext

import static org.junit.Assert.*

import org.junit.Test


class ProcessNodeTest extends GroovyTestCase {

    AWSCredentials awsCredentials

    void setUp() {
        this.awsCredentials = AWSCredentialsFactory.getInstance(AWSCredentialsFactory.AWS, AWSCredentialsFactory.TEST)
    }
	
	@Test
	void testCreateInstance_1() {

		def processNodeSpecObj = [
			type: "sequence",
			something: "extra",
			nodes: [[
					type: "action",
					action: "com.kloudminions.process.TestCloudActionNodeMinion:doSomething"
				],[
					type: "action",
					action: "com.kloudminions.process.TestCloudActionNodeMinion:saySomething"
				]]
		] // END processNodeSpecObj


		GlobalContext globalContext = new GlobalContext(this.awsCredentials)

		/*
		 * Method Under Test
		 */
		ProcessNode pn = ProcessNode.createInstance(processNodeSpecObj, globalContext)
		assertNotNull(pn)

		
		assertNotNull(pn)
		SequenceNode sn = (SequenceNode) pn
		assertEquals("sequence", sn.getTypeDefinition())
		assertEquals(2, sn.getNodeList().size())
		
		ProcessNode child_pn1 = sn.getNodeList().get(0)
		assertEquals("action", child_pn1.getTypeDefinition())
		assertEquals("com.kloudminions.process.ActionNode", child_pn1.class.name)
		
		ProcessNode child_pn2 = sn.getNodeList().get(1)
		assertEquals("action", child_pn2.getTypeDefinition())
		assertEquals("com.kloudminions.process.ActionNode", child_pn2.class.name)
		
		assertEquals("extra", pn.getLocalContext().get("something"))

		
	} // END testCreate_1 Method
	
	
	
	
} // END ProcessNodeTest Class
