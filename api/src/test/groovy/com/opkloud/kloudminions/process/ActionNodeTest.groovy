package com.opkloud.kloudminions.process

import com.amazonaws.auth.AWSCredentials
import com.kloudminions.auth.AWSCredentialsFactory
import com.kloudminions.context.LocalContext
import org.junit.Test

import com.kloudminions.context.GlobalContext

class ActionNodeTest extends GroovyTestCase {

    AWSCredentials awsCredentials

    void setUp() {
        this.awsCredentials = AWSCredentialsFactory.getInstance(AWSCredentialsFactory.AWS, AWSCredentialsFactory.TEST)
    }

	@Test
	void testCreateInstance_1() {

		def actionNodeSpecObj = [
			type: "action",
			action: "com.kloudminions.process.TestCloudActionNodeMinion:doSomething",
			something: "extra"
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", actionNodeSpecObj)

		/*
		 * Method Under Test
		 */

		def actionNode = ActionNode.createInstance(actionNodeSpecObj, localContext)

		assertNotNull(actionNode)
		assertEquals("action", actionNode.getTypeDefinition())
		assertEquals("com.kloudminions.process.TestCloudActionNodeMinion", actionNode.getMinionClassName())
		assertEquals("doSomething", actionNode.getMinionActionName())
		assertEquals("com.kloudminions.process.TestCloudActionNodeMinion", actionNode.getMinionObject().class.name)
		assertEquals("extra", actionNode.getLocalContext().get("something"))
		

	} // END testCreate_1 Method

	
	@Test
	void testCreateInstance_2() {

		def actionNodeSpecObj = [
			type: "action",
			action: "test_action_cloud_minion:doSomething"
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", actionNodeSpecObj)
		
		
		/*
		 * Method Under Test
		 */

		ActionNode actionNode = ActionNode.createInstance(actionNodeSpecObj, localContext)

		assertNotNull(actionNode)
		assertEquals("action", actionNode.getTypeDefinition())
		assertEquals("com.kloudminions.process.TestCloudActionNodeMinion", actionNode.getMinionClassName())
		assertEquals("doSomething", actionNode.getMinionActionName())
		assertEquals("com.kloudminions.process.TestCloudActionNodeMinion", actionNode.getMinionObject().class.name)

	} // END testCreate_2 Method

	
	@Test
	void testCreateInstance_shouldFail_1_type_empty_string() {

		def actionNodeSpecObj = [
			type: "",
			action: "test_action_cloud_minion:doSomething"
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", actionNodeSpecObj)


		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = ActionNode.createInstance(actionNodeSpecObj, localContext)
		}

	} // END testCreateInstance_shouldFail_1_type_empty_string Method
	
	
	@Test
	void testCreateInstance_shouldFail_1_type_null() {

		def actionNodeSpecObj = [
			type: null,
			action: "test_action_cloud_minion:doSomething"
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", actionNodeSpecObj)


		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = ActionNode.createInstance(actionNodeSpecObj, localContext)
		}

	} // END testCreateInstance_shouldFail_1_type_null Method
	
	
	@Test
	void testCreateInstance_shouldFail_1_type_all_spaces() {

		def actionNodeSpecObj = [
			type: "     ",
			action: "test_action_cloud_minion:doSomething"
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", actionNodeSpecObj)


		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = ActionNode.createInstance(actionNodeSpecObj, localContext)
		}

	} // END testCreateInstance_shouldFail_1_type_all_spaces Method

	
	@Test
	void testCreateInstance_shouldFail_type_notequal_action_1() {

		def actionNodeSpecObj = [
			type: "ACTion",
			action: "test_action_cloud_minion:doSomething"
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", actionNodeSpecObj)


		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = ActionNode.createInstance(actionNodeSpecObj, localContext)
		}

	} // END Method
	
	@Test
	void testCreateInstance_shouldFail_type_notequal_action_2() {

		def actionNodeSpecObj = [
			type: "sequence",
			action: "test_action_cloud_minion:doSomething"
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", actionNodeSpecObj)


		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = ActionNode.createInstance(actionNodeSpecObj, localContext)
		}

	} // END Method
	
	@Test
	void testCreateInstance_shouldFail_action_is_null() {

		def actionNodeSpecObj = [
			type: "action",
			action: null
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", actionNodeSpecObj)


		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = ActionNode.createInstance(actionNodeSpecObj, localContext)
		}

	} // END Method
	
	
	@Test
	void testCreateInstance_shouldFail_action_is_empty_string() {

		def actionNodeSpecObj = [
			type: "action",
			action: ""
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", actionNodeSpecObj)

		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = ActionNode.createInstance(actionNodeSpecObj, localContext)
		}

	} // END Method
	
	
	@Test
	void testCreateInstance_shouldFail_action_is_all_spaces() {

		def actionNodeSpecObj = [
			type: "action",
			action: "    "
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", actionNodeSpecObj)

		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = ActionNode.createInstance(actionNodeSpecObj, localContext)
		}

	} // END Method
	
	
	@Test
	void testCreateInstance_shouldFail_context_is_null() {

		def actionNodeSpecObj = [
			type: "action",
			action: "test_action_cloud_minion:doSomething"
		]


		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = ActionNode.createInstance(actionNodeSpecObj, null)
		}

	} // END Method
	
	
	@Test
	void testExecute_1() {

		def actionNodeSpecObj = [
			type: "action",
			action: "com.kloudminions.process.TestCloudActionNodeMinion:doSomething"
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", actionNodeSpecObj)

		
		ActionNode actionNode = ActionNode.createInstance(actionNodeSpecObj, localContext)
		assertNotNull(actionNode)

		/*
		 * Method Under Test
		 */
		actionNode.execute()
		
		LocalContext newLocalContext = actionNode.getLocalContext()
		
		assertEquals("Cool!", newLocalContext.get("didSomething"))
		
	} // END testExecute_1 Method
	
	
	@Test
	void testExecute_2() {

		def actionNodeSpecObj = [
			type: "action",
			action: "com.kloudminions.process.TestCloudActionNodeMinion:saySomething"
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", actionNodeSpecObj)

		
		ActionNode actionNode = ActionNode.createInstance(actionNodeSpecObj, localContext)
		assertNotNull(actionNode)

		/*
		 * Method Under Test
		 */
		actionNode.execute()
		
		LocalContext newLocalContext = actionNode.getLocalContext()
		
		assertEquals("Hello!", newLocalContext.get("saidSomething"))
		
	} // END testExecute_2 Method
	
} // END ActionNodeTest Class
