package com.opkloud.kloudminions.process

import com.amazonaws.auth.AWSCredentials
import com.kloudminions.auth.AWSCredentialsFactory
import com.kloudminions.context.GlobalContext

import static org.junit.Assert.*
import groovy.json.JsonSlurper
import org.junit.Test

class ProcessTest extends GroovyTestCase {

    AWSCredentials awsCredentials

    void setUp() {
        this.awsCredentials = AWSCredentialsFactory.getInstance(AWSCredentialsFactory.AWS, AWSCredentialsFactory.TEST)
    }

    /**
     *  If developers want a quick way to craft process specifications this test will provide all of the tools that
     *  you need to get started quickly
     */

    @Test
	void testExecute_UsingFiles() {
		
		JsonSlurper jsonSlurper = new JsonSlurper()

        String processSpecString = """
            {
                "type": "sequence",
                "nodes": [{
                    "type": "action",
                    "action": "cloud_formation_minion:deleteStack"
                }]
            }
			"""
		Map processSpecMap = jsonSlurper.parseText(processSpecString)


		String globalContextString = """
			{
			    "aws_region": "us-east-1",
				"aws_stack_name": "Does Not Exist Anywhere"
			}
			"""
		Map globalContextMap = jsonSlurper.parseText(globalContextString)


		Process process = Process.getBuilder().setGlobalContextMap(globalContextMap)
											.setAWSCredentials(this.awsCredentials)
											.setProcessSpec(processSpecMap)
											.build()


		assertNotNull(process)



		
		
		/*
		 * Method Under Test
		 */
		process.execute()
		
		
		SequenceNode sn = process.getRootProcessNode()

		assertNotNull(sn)
		
	} // END testExecute_UsingFiles Method




	@Test
	void testCreate_UsingFileStrings() {

		JsonSlurper jsonSlurper = new JsonSlurper()

		String processSpecString = """
            {
                "type": "sequence",
                "nodes": [{
                    "type": "action",
                    "action": "cloud_formation_minion:deleteStack"
                }]
            }
			"""
		Map processSpecMap = jsonSlurper.parseText(processSpecString)


		String globalContextString = """
			{
			    "aws_region": "us-east-1",
				"aws_stack_name": "Does Not Exist Anywhere"
			}
			"""
		Map globalContextMap = jsonSlurper.parseText(globalContextString)


		Process process = Process.createInstance(processSpecMap, this.awsCredentials, globalContextMap)


		assertNotNull(process)


		/*
		 * Method Under Test
		 */
		process.execute()


		SequenceNode sn = process.getRootProcessNode()

		assertNotNull(sn)

	} // END testExecute_UsingFiles Method


	
} // END ProcessNodeTest Class
