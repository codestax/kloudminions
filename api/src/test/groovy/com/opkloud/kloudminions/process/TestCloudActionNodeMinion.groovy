package com.opkloud.kloudminions.process

import groovy.transform.InheritConstructors

import com.kloudminions.context.GlobalContext
import com.kloudminions.minions.AbstractCloudMinion
import com.kloudminions.registry.MinReg

/*
 *  FOR TESTING PURPOSES ONLY!!!!!!
 *  Class used to implement and instantiate the abstract class
 */
@InheritConstructors
public class TestCloudActionNodeMinion extends AbstractCloudMinion {

	static {
		MinReg.add("test_action_cloud_minion", "com.kloudminions.process.TestCloudActionNodeMinion")
	}
	
	public TestCloudActionNodeMinion(GlobalContext context) {
		super(context)
	}

	@Override
	void execute() {
		// TODO Auto-generated method stub

	}
	
	void doSomething() {
		
		this.context.add("didSomething", "Cool!")

	}
	
	void saySomething() {
		
		this.context.add("saidSomething", "Hello!")

	}
} // END TestCloudActionNodeMinion Inner Class