package com.opkloud.kloudminions.process

import com.amazonaws.auth.AWSCredentials
import com.kloudminions.auth.AWSCredentialsFactory
import com.kloudminions.context.GlobalContext
import com.kloudminions.context.LocalContext
import org.junit.Test

class SequenceNodeTest extends GroovyTestCase {

    AWSCredentials awsCredentials

    void setUp() {
        this.awsCredentials = AWSCredentialsFactory.getInstance(AWSCredentialsFactory.AWS, AWSCredentialsFactory.TEST)
    }
	
	@Test
	void testCreateInstance_1() {

		def sequenceNodeSpecObj = [
			type: "sequence",
			something: "extra",
			nodes: []
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", sequenceNodeSpecObj)

		/*
		 * Method Under Test
		 */
		SequenceNode sequenceNode = SequenceNode.createInstance(sequenceNodeSpecObj, localContext)

		assertNotNull(sequenceNode)
		assertEquals("sequence", sequenceNode.getTypeDefinition())
		assertEquals(0, sequenceNode.getNodeList().size())
		assertEquals("extra", sequenceNode.getLocalContext().get("something"))

	} // END testCreate_1 Method


	@Test
	void testCreateInstance_2() {

		def sequenceNodeSpecObj = [
			type: "sequence",
			something: "extra",
			nodes: [[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:doSomething"
			],[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:saySomething"
			]]
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", sequenceNodeSpecObj)

		/*
		 * Method Under Test
		 */
		SequenceNode sequenceNode = SequenceNode.createInstance(sequenceNodeSpecObj, localContext)

		assertNotNull(sequenceNode)
		assertEquals("sequence", sequenceNode.getTypeDefinition())
		assertEquals(2, sequenceNode.getNodeList().size())
		
		ProcessNode pn1 = sequenceNode.getNodeList().get(0)
		assertEquals("action", pn1.getTypeDefinition())
		assertEquals("com.kloudminions.process.ActionNode", pn1.class.name)
		
		ProcessNode pn2 = sequenceNode.getNodeList().get(1)
		assertEquals("action", pn2.getTypeDefinition())
		assertEquals("com.kloudminions.process.ActionNode", pn2.class.name)
		
		assertEquals("extra", sequenceNode.getLocalContext().get("something"))

	} // END testCreate_2 Method

	
	@Test
	void testCreateInstance_shouldFail_1_type_empty_string() {

		def sequenceNodeSpecObj = [
			type: "",
			nodes: [[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:doSomething"
			],[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:saySomething"
			]]
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", sequenceNodeSpecObj)

		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = SequenceNode.createInstance(sequenceNodeSpecObj, localContext)
		}

	} // END testCreateInstance_shouldFail_1_type_empty_string Method
	
	
	@Test
	void testCreateInstance_shouldFail_1_type_null() {
		
		def sequenceNodeSpecObj = [
			type: null,
			nodes: [[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:doSomething"
			],[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:saySomething"
			]]
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", sequenceNodeSpecObj)

		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = SequenceNode.createInstance(sequenceNodeSpecObj, localContext)
		}
		
	} // END testCreateInstance_shouldFail_1_type_null Method
	
	
	@Test
	void testCreateInstance_shouldFail_1_type_all_spaces() {

		def sequenceNodeSpecObj = [
			type: "     ",
			nodes: [[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:doSomething"
			],[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:saySomething"
			]]
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", sequenceNodeSpecObj)
		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = SequenceNode.createInstance(sequenceNodeSpecObj, localContext)
		}

	} // END testCreateInstance_shouldFail_1_type_all_spaces Method

	
	@Test
	void testCreateInstance_shouldFail_type_notequal_sequence_1() {

		def sequenceNodeSpecObj = [
			type: "SEQuence",
			nodes: [[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:doSomething"
			],[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:saySomething"
			]]
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", sequenceNodeSpecObj)

		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = SequenceNode.createInstance(sequenceNodeSpecObj, localContext)
		}

	} // END Method
	
	
	@Test
	void testCreateInstance_shouldFail_type_notequal_action_2() {

		def sequenceNodeSpecObj = [
			type: "action",
			nodes: [[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:doSomething"
			],[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:saySomething"
			]]
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", sequenceNodeSpecObj)

		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = SequenceNode.createInstance(sequenceNodeSpecObj, localContext)
		}

	} // END Method
	
	
	@Test
	void testCreateInstance_shouldFail_nodes_is_null() {

		def sequenceNodeSpecObj = [
			type: "sequence",
			nodes: null
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", sequenceNodeSpecObj)


		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = SequenceNode.createInstance(sequenceNodeSpecObj, localContext)
		}

	} // END Method
	
	
	@Test
	void testCreateInstance_shouldFail_nodes_is_not_a_list_or_array() {

		def sequenceNodeSpecObj = [
			type: "sequence",
			nodes: ""
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", sequenceNodeSpecObj)


		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = SequenceNode.createInstance(sequenceNodeSpecObj, localContext)
		}

	} // END Method
	
	
	@Test
	void testCreateInstance_shouldFail_context_is_null() {

		def sequenceNodeSpecObj = [
			type: "sequence",
			nodes: []
		]


		/*
		 * Method Under Test
		 */
		shouldFail (IllegalArgumentException) {
			def actionNode = SequenceNode.createInstance(sequenceNodeSpecObj, null)
		}

	} // END Method
	
	
	@Test
	void testExecute_1() {

		def sequenceNodeSpecObj = [
			type: "sequence",
			something: "extra",
			nodes: [[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:doSomething"
			],[
				type: "action",
				action: "com.kloudminions.process.TestCloudActionNodeMinion:saySomething"
			]]
		]

		GlobalContext globalContext = new GlobalContext(this.awsCredentials)
		LocalContext localContext = globalContext.createLocalContext("nodeA", sequenceNodeSpecObj)

		SequenceNode sequenceNode = SequenceNode.createInstance(sequenceNodeSpecObj, localContext)
		assertNotNull(sequenceNode)
		
		/*
		 * Method Under Test
		 */
		sequenceNode.execute()
		
		
		assertEquals("sequence", sequenceNode.getTypeDefinition())
		assertEquals(2, sequenceNode.getNodeList().size())
		
		ProcessNode pn1 = sequenceNode.getNodeList().get(0)
		assertEquals("action", pn1.getTypeDefinition())
		assertEquals("com.kloudminions.process.ActionNode", pn1.class.name)
		assertEquals("Cool!", pn1.getLocalContext().get("didSomething"))
		
		ProcessNode pn2 = sequenceNode.getNodeList().get(1)
		assertEquals("action", pn2.getTypeDefinition())
		assertEquals("com.kloudminions.process.ActionNode", pn2.class.name)
		assertEquals("Hello!", pn2.getLocalContext().get("saidSomething"))
		
	} // END testExecute_1 Method
	
	
} // END SequenceNodeTest Class
