package com.opkloud.kloudminions.process

import com.amazonaws.auth.AWSCredentials
import com.kloudminions.auth.AWSCredentialsFactory
import groovy.json.JsonSlurper
import org.junit.Test

import static org.junit.Assert.assertNotNull;

class ProcessBuilderTest extends GroovyTestCase {

    AWSCredentials awsCredentials

    void setUp() {
        this.awsCredentials = AWSCredentialsFactory.getInstance(AWSCredentialsFactory.AWS, AWSCredentialsFactory.TEST)
    }

    /**
     *  If developers want a quick way to craft process specifications this test will provide all of the tools that
     *  you need to get started quickly
     */

    @Test
    void testBuild_UsingStrings() {

        String processSpecString = """
            {
                "type": "sequence",
                "nodes": [{
                    "type": "action",
                    "action": "cloud_formation_minion:deleteStack"
                }]
            }
			"""


        String globalContextMapString = """
			{
			    "aws_region": "us-east-1",
				"aws_stack_name": "Does Not Exist Anywhere"
			}
			"""

        /*
            Method under test
         */
        ProcessBuilder processBuilder = new ProcessBuilder()
        processBuilder.setProcessSpec(processSpecString)
        processBuilder.setGlobalContextMap(globalContextMapString)
        processBuilder.setAWSCredentials(this.awsCredentials)

        Process process = processBuilder.build()
        assertNotNull(process)


        /*
         * Method Further Under Test :-)
         */
        process.execute()


        SequenceNode sn = process.rootProcessNode
        assertNotNull(sn)

    } // END testBuild_UsingStrings Method



    @Test
    void testBuild_UsingMaps() {

        JsonSlurper jsonSlurper = new JsonSlurper()

        String processSpecString = """
            {
                "type": "sequence",
                "nodes": [{
                    "type": "action",
                    "action": "cloud_formation_minion:deleteStack"
                }]
            }
			"""
        Map processSpecMap = jsonSlurper.parseText(processSpecString)


        String processContextString = """
			{
			    "aws_region": "us-east-1",
				"aws_stack_name": "Does Not Exist Anywhere"
			}
			"""
        Map processContextMap = jsonSlurper.parseText(processContextString)


        /*
            Method under test
        */
        ProcessBuilder processBuilder = new ProcessBuilder()
        processBuilder.setProcessSpec(processSpecMap)
        processBuilder.setGlobalContextMap(processContextMap)
        processBuilder.setAWSCredentials(this.awsCredentials)

        Process process = processBuilder.build()
        assertNotNull(process)


        /*
         * Method Further Under Test :-)
         */
        process.execute()


        SequenceNode sn = process.rootProcessNode
        assertNotNull(sn)

    } // END testBuild_UsingStrings Method



	@Test
	void testBuild_UsingStrings_ChainedCalls() {

		JsonSlurper jsonSlurper = new JsonSlurper()

		String processSpecString = """
            {
                "type": "sequence",
                "nodes": [{
                    "type": "action",
                    "action": "cloud_formation_minion:deleteStack"
                }]
            }
			"""
		Map processSpecMap = jsonSlurper.parseText(processSpecString)


		String processContextString = """
			{
			    "aws_region": "us-east-1",
				"aws_stack_name": "Does Not Exist Anywhere"
			}
			"""
		Map processContextMap = jsonSlurper.parseText(processContextString)


		/*
			Method under test
		*/
		Process process = new ProcessBuilder().setProcessSpec(processSpecMap)
												.setGlobalContextMap(processContextMap)
												.setAWSCredentials(this.awsCredentials)
												.build()

		assertNotNull(process)


		/*
		 * Method Further Under Test :-)
		 */
		process.execute()


		SequenceNode sn = process.rootProcessNode
		assertNotNull(sn)

	} // END testBuild_UsingStrings_ChainedCalls Method





} // END ProcessBuilderTest Class

