package com.opkloud.kloudminions.minions

import com.amazonaws.auth.AWSCredentials
import com.kloudminions.auth.AWSCredentialsFactory
import org.junit.Test

import com.kloudminions.context.GlobalContext


public class AbstractCloudMinionTest extends GroovyTestCase {

    AWSCredentials awsCredentials

    void setUp() {
        this.awsCredentials = AWSCredentialsFactory.getInstance(AWSCredentialsFactory.AWS, AWSCredentialsFactory.TEST)
    }
	
	@Test
	void testConstructor_awsCredentials_1() {

		GlobalContext context = new GlobalContext(this.awsCredentials)
		
		/*
		 * Method under test
		 */
		TestCloudMinion testCloudMinion = new TestCloudMinion(context)
		
		assertNotNull testCloudMinion
		
		GlobalContext resultContext = testCloudMinion.getContext()
		
		assertNotNull(resultContext)
		assertNotNull(resultContext.getAWSCredentials())
		
	}


    @Test
    void testConstructor_awsCredentials_contextMap_1() {

        GlobalContext context = new GlobalContext(this.awsCredentials, [foo: "bar"])

        /*
         * Method under test
         */
        TestCloudMinion testCloudMinion = new TestCloudMinion(context)

        assertNotNull testCloudMinion

        GlobalContext resultContext = testCloudMinion.getContext()

        assertNotNull(resultContext)
        assertNotNull(resultContext.getAWSCredentials())
        assertEquals("bar", resultContext.get("foo"))

    }


	/**
	 * Test to see if Minion is prevented from being instantiated with a null context
	 */
	@Test
	void testConstructor_IllegalArgument_1() {

		/*
		 * Method under test
		 */
		shouldFail(IllegalArgumentException) {
			TestCloudMinion testCloudMinion = new TestCloudMinion(null)
		}
		
	}
	
	
	/**
	 * Test to see if Minion is prevented from being instantiated without a context
	 */
	@Test
	void testConstructor_IllegalArgument_2() {

		/*
		 * Method under test
		 */
		shouldFail(IllegalArgumentException) {
			TestCloudMinion testCloudMinion = new TestCloudMinion()
		}
		
	}
	
	
	@Test
	void testSetContext() {

        GlobalContext context = new GlobalContext(this.awsCredentials, [foo: "bar"])
		
		TestCloudMinion testCloudMinion = new TestCloudMinion(context)
		assertNotNull testCloudMinion 
		
		GlobalContext resultContext = testCloudMinion.getContext();
		assertNotNull resultContext
        assertEquals("bar", resultContext.get("foo"))

		
		GlobalContext newContext = new GlobalContext(this.awsCredentials, [
			"new_attrib" : "HELLO"
		])
		
		
		/*
		 * Method under test
		 */
		testCloudMinion.setContext(newContext)
		
		
		GlobalContext new_context = testCloudMinion.getContext();
		
		assertNotNull new_context
		assertEquals ("HELLO", new_context.get("new_attrib"))
        assertNull(new_context.get("foo"))
		
	}
	
	

	
} // END AbstractCloudMinionTest Class