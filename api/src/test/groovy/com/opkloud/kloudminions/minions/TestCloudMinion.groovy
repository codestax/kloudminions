package com.opkloud.kloudminions.minions

import groovy.transform.InheritConstructors;

import com.kloudminions.context.GlobalContext;

/* ======================================================== */
/* ==== TEST SUPPORT CLASSES                           === */
/* ======================================================== */

/*
 * Test class used to implement and instantiate the abstract class
 */
@KloudMinion(name="test_minion")
@InheritConstructors
class TestCloudMinion extends AbstractCloudMinion {

	public TestCloudMinion(GlobalContext context) {
		super(context)
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub

	}

}
