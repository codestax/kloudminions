package com.opkloud.kloudminions.minions

import org.reflections.Reflections
import org.reflections.scanners.SubTypesScanner
import org.reflections.scanners.TypeAnnotationsScanner
import org.reflections.util.ClasspathHelper
import org.reflections.util.ConfigurationBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Specification

/**
 * Created by rashadmoore on 5/5/16.
 */
class KloudMinionTest extends Specification {

	private static final Logger log = LoggerFactory.getLogger(KloudMinionTest.class)


	def "scan the classpath for the KloudMinion annotion"() {


		given:
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
				.setUrls(ClasspathHelper.forJavaClassPath())
				.setScanners(new SubTypesScanner(), new TypeAnnotationsScanner())


		Reflections reflections = new Reflections(configurationBuilder)


		when:
		Set<Class<?>> singletons = reflections.getTypesAnnotatedWith(KloudMinion.class)
		singletons.each {minionClass ->

			KloudMinion kloudMinionAnnotation = minionClass.getAnnotation(KloudMinion.class)
			log.debug("Class Name: " + minionClass.getName() + "   ---  Minion Value: " + kloudMinionAnnotation.name())


		}



		then:
		singletons != null
		singletons.size() >= 5 // This is just to make sure that there are some classes found...



	}




}
