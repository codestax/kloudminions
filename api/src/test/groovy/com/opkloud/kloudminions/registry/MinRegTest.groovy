package com.opkloud.kloudminions.registry

import com.amazonaws.auth.AWSCredentials
import com.kloudminions.auth.AWSCredentialsFactory
import com.kloudminions.context.GlobalContext

import static org.junit.Assert.*

import org.junit.Test


import com.kloudminions.minions.TestCloudMinion

class MinRegTest extends GroovyTestCase{

    AWSCredentials awsCredentials

    void setUp() {
        this.awsCredentials = AWSCredentialsFactory.getInstance(AWSCredentialsFactory.AWS, AWSCredentialsFactory.TEST)
    }
	
	@Test
	void test_get_1() {
		
		/*
		 * Method under test
		 */
		String minionClassName = MinReg.get("test_minion")
		
		
		assertEquals("com.kloudminions.minions.TestCloudMinion", minionClassName)
		
	}
	
	
	@Test
	void test_add_String_String_1() {
		
		/*
		 * Method under test
		 */
		MinReg.add("a_new_minion", "com.kloudminions.minions.ANewMinion")
		
		String minionClassName = MinReg.get("a_new_minion")
		assertEquals("com.kloudminions.minions.ANewMinion", minionClassName)
	}
	
	
	@Test
	void test_add_String_String_shouldFail_1() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add(null, "com.kloudminions.minions.ANewMinion")
		}
	}
	
	@Test
	void test_add_String_String_shouldFail_2() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add("", "com.kloudminions.minions.ANewMinion")
		}
	}
	
	
	@Test
	void test_add_String_String_shouldFail_3() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add("    ", "com.kloudminions.minions.ANewMinion")
		}
	}
	
	
	@Test
	void test_add_String_String_shouldFail_4() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add("a_new_minion", null)
		}
	}
	
	@Test
	void test_add_String_String_shouldFail_5() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add("a_new_minion", "")
		}
	}
	
	@Test
	void test_add_String_String_shouldFail_6() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add("a_new_minion", "   ")
		}
	}
	
	
	@Test
	void test_add_String_Class_1() {
		
		
		/*
		 * Method under test
		 */
		MinReg.add("a_new_minion", com.kloudminions.minions.TestCloudMinion)
		
		String minionClassName = MinReg.get("a_new_minion")
		assertEquals("com.kloudminions.minions.TestCloudMinion", minionClassName)
	}
	
	
	@Test
	void test_add_String_Class_shouldFail_1() {
		
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add(null, com.kloudminions.minions.TestCloudMinion)
		}
	}
	
	@Test
	void test_add_String_Class_shouldFail_2() {
		
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add("", com.kloudminions.minions.TestCloudMinion)
		}
	}
	
	
	@Test
	void test_add_String_Class_shouldFail_3() {
		
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add("    ", com.kloudminions.minions.TestCloudMinion)
		}
	}
	
	
	@Test
	void test_add_String_Class_shouldFail_4() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add("a_new_minion", null)
		}
	}
	
	
	@Test
	void test_add_String_Minion_1() {
		
		/*
		 * Fixture
		 */
		GlobalContext context = new GlobalContext(this.awsCredentials, [
			"cloud_provider" : "AWS"
		])
		
		TestCloudMinion testCloudMinion = new TestCloudMinion(context)
		
		assertNotNull testCloudMinion
		
		
		/*
		 * Method under test
		 */
		MinReg.add("a_new_minion", testCloudMinion)
		
		String minionClassName = MinReg.get("a_new_minion")
		assertEquals("com.kloudminions.minions.TestCloudMinion", minionClassName)
	}
	
	@Test
	void test_add_String_Minion_shouldFail_1() {
		
		/*
		 * Fixture
		 */
        GlobalContext context = new GlobalContext(this.awsCredentials, [
			"cloud_provider" : "AWS"
		])
		
		TestCloudMinion testCloudMinion = new TestCloudMinion(context)
		assertNotNull testCloudMinion
		
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add(null, testCloudMinion)
		}
	}
	
	@Test
	void test_add_String_Minion_shouldFail_2() {
		
		/*
		 * Fixture
		 */
        GlobalContext context = new GlobalContext(this.awsCredentials, [
			"cloud_provider" : "AWS"
		])
		
		TestCloudMinion testCloudMinion = new TestCloudMinion(context)
		assertNotNull testCloudMinion
		
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add("", testCloudMinion)
		}
	}
	
	
	@Test
	void test_add_String_Minion_shouldFail_3() {
		
		/*
		 * Fixture
		 */
        GlobalContext context = new GlobalContext(this.awsCredentials, [
			"cloud_provider" : "AWS"
		])
		
		TestCloudMinion testCloudMinion = new TestCloudMinion(context)
		assertNotNull testCloudMinion
		
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add("    ", testCloudMinion)
		}
	}
	
	
	@Test
	void test_add_String_Minion_shouldFail_4() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinReg.add("a_new_minion", null)
		}
	}
	
} // END MinRegTest Class
