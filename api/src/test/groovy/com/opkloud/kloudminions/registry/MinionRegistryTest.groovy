package com.opkloud.kloudminions.registry

import com.amazonaws.auth.AWSCredentials
import com.kloudminions.auth.AWSCredentialsFactory

import static org.junit.Assert.*

import org.junit.Test

import com.kloudminions.context.GlobalContext
import com.kloudminions.minions.TestCloudMinion

class MinionRegistryTest extends GroovyTestCase{

    AWSCredentials awsCredentials

    void setUp() {
        this.awsCredentials = AWSCredentialsFactory.getInstance(AWSCredentialsFactory.AWS, AWSCredentialsFactory.TEST)
		MinionRegistry.initializeRegister()
    }


	@Test
	void test_GetMinion_1() {



		/*
		 * Method under test
		 */
		String minionClassName = MinionRegistry.getMinionClassName("test_minion")
		
		assertEquals("com.kloudminions.minions.TestCloudMinion", minionClassName)
	}
	
	
	@Test
	void test_RegisterMinionType_String_String_1() {
		
		/*
		 * Method under test
		 */
		MinionRegistry.registerMinionType("a_new_minion", "com.kloudminions.minions.ANewMinion")
		
		String minionClassName = MinionRegistry.getMinionClassName("a_new_minion")
		assertEquals("com.kloudminions.minions.ANewMinion", minionClassName)
	}
	
	
	@Test
	void test_RegisterMinionType_String_String_shouldFail_1() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType(null, "com.kloudminions.minions.ANewMinion")
		}
	}
	
	
	@Test
	void test_RegisterMinionType_String_String_shouldFail_2() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType("", "com.kloudminions.minions.ANewMinion")
		}
	}
	
	
	@Test
	void test_RegisterMinionType_String_String_shouldFail_3() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType("    ", "com.kloudminions.minions.ANewMinion")
		}
	}
	
	
	@Test
	void test_RegisterMinionType_String_String_shouldFail_4() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType("a_new_minion", null)
		}
	}
	
	
	@Test
	void test_RegisterMinionType_String_String_shouldFail_5() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType("a_new_minion", "")
		}
	}
	
	
	@Test
	void test_RegisterMinionType_String_String_shouldFail_6() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType("a_new_minion", "   ")
		}
	}
	
	
	@Test
	void test_RegisterMinionType_String_Class_1() {
		
		
		/*
		 * Method under test
		 */
		MinionRegistry.registerMinionType("a_new_minion", com.kloudminions.minions.TestCloudMinion)
		
		String minionClassName = MinionRegistry.getMinionClassName("a_new_minion")
		assertEquals("com.kloudminions.minions.TestCloudMinion", minionClassName)
	}
	
	
	@Test
	void test_RegisterMinionType_String_Class_shouldFail_1() {
		
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType(null, com.kloudminions.minions.TestCloudMinion)
		}
	}
	
	@Test
	void test_RegisterMinionType_String_Class_shouldFail_2() {
		
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType("", com.kloudminions.minions.TestCloudMinion)
		}
	}
	
	
	@Test
	void test_RegisterMinionType_String_Class_shouldFail_3() {
		
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType("    ", com.kloudminions.minions.TestCloudMinion)
		}
	}
	
	
	@Test
	void test_RegisterMinionType_String_Class_shouldFail_4() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType("a_new_minion", null)
		}
	}
	
	
	@Test
	void test_RegisterMinionType_String_Minion_1() {
		
		/*
		 * Fixture
		 */
		GlobalContext context = new GlobalContext(this.awsCredentials)
		
		TestCloudMinion testCloudMinion = new TestCloudMinion(context)
		
		assertNotNull testCloudMinion
		
		
		/*
		 * Method under test
		 */
		MinionRegistry.registerMinionType("a_new_minion", testCloudMinion)
		
		String minionClassName = MinionRegistry.getMinionClassName("a_new_minion")
		assertEquals("com.kloudminions.minions.TestCloudMinion", minionClassName)
	}
	
	
	@Test
	void test_RegisterMinionType_String_Minion_shouldFail_1() {
		
		/*
		 * Fixture
		 */
		GlobalContext context = new GlobalContext(this.awsCredentials)
		
		TestCloudMinion testCloudMinion = new TestCloudMinion(context)
		assertNotNull testCloudMinion
		
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType(null, testCloudMinion)
		}
	}
	
	
	@Test
	void test_RegisterMinionType_String_Minion_shouldFail_2() {
		
		/*
		 * Fixture
		 */
		GlobalContext context = new GlobalContext(this.awsCredentials)
		
		TestCloudMinion testCloudMinion = new TestCloudMinion(context)
		assertNotNull testCloudMinion
		
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType("", testCloudMinion)
		}
	}
	
	
	@Test
	void test_RegisterMinionType_String_Minion_shouldFail_3() {
		
		/*
		 * Fixture
		 */
		GlobalContext context = new GlobalContext(this.awsCredentials)
		
		TestCloudMinion testCloudMinion = new TestCloudMinion(context)
		assertNotNull testCloudMinion
		
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType("    ", testCloudMinion)
		}
	}
	
	
	@Test
	void test_RegisterMinionType_String_Minion_shouldFail_4() {
		
		/*
		 * Method under test
		 */
		shouldFail (IllegalArgumentException) {
			MinionRegistry.registerMinionType("a_new_minion", null)
		}
	}
	
} // END MinionRegistryTest Class
