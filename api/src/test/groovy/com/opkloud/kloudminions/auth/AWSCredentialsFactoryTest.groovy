package com.opkloud.kloudminions.auth

import com.amazonaws.auth.AWSCredentials
import org.junit.Test;


/**
 * Created by rashadmoore on 3/31/16.
 */
class AWSCredentialsFactoryTest extends GroovyTestCase {


    void setUp() {

    }

    @Test
    void testGetInstance_platform_environment_1() {

        /*
            Fixture
         */
        String platform = AWSCredentialsFactory.AWS
        String environment = AWSCredentialsFactory.TEST


        /*
            Method under test
         */
        AWSCredentials awsCredentials = AWSCredentialsFactory.getInstance(platform, environment)


        assertEquals(20, awsCredentials.AWSAccessKeyId.length())
        assertEquals(40, awsCredentials.AWSSecretKey.length())

    }


    @Test
    void testGetInstance_environment_1() {

        /*
            Fixture
         */
        String environment = AWSCredentialsFactory.TEST


        /*
            Method under test
         */
        AWSCredentials awsCredentials = AWSCredentialsFactory.getInstance(environment)


        assertEquals(20, awsCredentials.AWSAccessKeyId.length())
        assertEquals(40, awsCredentials.AWSSecretKey.length())

    }



} // END AWSCredentialsFactoryTest Class
