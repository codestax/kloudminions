package com.opkloud.kloudminions.context

import com.amazonaws.auth.AWSCredentials
import com.kloudminions.auth.AWSCredentialsFactory
import spock.lang.Shared
import spock.lang.Specification

/**
 * Created by rashadmoore on 4/25/16.
 */
class LocalContextSpec extends Specification {


	@Shared
	AWSCredentials awsCredentials  = AWSCredentialsFactory.getInstance(AWSCredentialsFactory.AWS, AWSCredentialsFactory.TEST)



	def "instantiate a simple LocalContext and test a few local and global context variables" () {

		given:
		String contextId = "nodeA"

		Map<String, Object> globalContextMap = [
				foo: "a global hello",
				bar: "a global world",
				baz: "Global Baz"
		]
		GlobalContext globalContext = new GlobalContext(this.awsCredentials, globalContextMap)

		Map<String, Object> localContextMap = [
		        vpcId: "123",
				subnetId: "abc",
				baz: "Local Baz"
		]


		when:
		LocalContext localContext = new LocalContext(contextId, globalContext)
		localContext.addAll(localContextMap)


		then:
		localContext.get("vpcId") == "123"
		localContext.get("subnetId") == "abc"
		localContext.get("baz") == "Local Baz"

		localContext.get("NOT THERE") == null

		localContext.get("foo") == "a global hello"
		localContext.get("bar") == "a global world"

	}


	def "test that references work within the local context on the first level" () {

		given:
		String contextId = "nodeA"

		Map<String, Object> globalContextMap = [
				foo: "a global hello",
				bar: "a global world",
				baz: "Global Baz"
		]
		GlobalContext globalContext = new GlobalContext(this.awsCredentials, globalContextMap)

		Map<String, Object> localContextMap = [
			vpcId: "123",
			subnetId: "abc",
			baz: "Local Baz",
			bat: [ref: "bar"]
		]


		when:
		LocalContext localContext = new LocalContext(contextId, globalContext)
		localContext.addAll(localContextMap)


		then:
		localContext.get("bat") == "a global world"
		localContext.get("vpcId") == "123"
		localContext.get("subnetId") == "abc"
		localContext.get("baz") == "Local Baz"

		localContext.get("NOT THERE") == null

		localContext.get("foo") == "a global hello"
		localContext.get("bar") == "a global world"

	}


	def "test that references work within the local context on the second level" () {

		given:
		String contextId = "nodeA"

		Map<String, Object> globalContextMap = [
				foo: "a global hello",
				bar: "a global world",
				baz: "Global Baz",
				son: "Son on a global level"
		]
		GlobalContext globalContext = new GlobalContext(this.awsCredentials, globalContextMap)

		Map<String, Object> localContextMap = [
				vpcId: "123",
				subnetId: "abc",
				baz: "Local Baz",
				config: [
					aidan: "cool",
					preston: [ref: "son"]
				],
				jasmine: "cute"
		]


		when:
		LocalContext localContext = new LocalContext(contextId, globalContext)
		localContext.addAll(localContextMap)


		then:
		localContext.get("config").preston == "Son on a global level"
		localContext.get("vpcId") == "123"
		localContext.get("subnetId") == "abc"
		localContext.get("baz") == "Local Baz"

		localContext.get("NOT THERE") == null

		localContext.get("foo") == "a global hello"
		localContext.get("bar") == "a global world"

	}


	def "test that references work within the local context on the fourth level" () {

		given:
		String contextId = "nodeA"

		Map<String, Object> globalContextMap = [
				foo: "a global hello",
				bar: "a global world",
				baz: "Global Baz",
				son: "Son on a global level",
				trees: "lots of trees"
		]
		GlobalContext globalContext = new GlobalContext(this.awsCredentials, globalContextMap)

		Map<String, Object> localContextMap = [
				vpcId: "123",
				subnetId: "abc",
				baz: "Local Baz",
				config: [
						aidan: "cool",
						preston: [
								trail: "rider",
								chex: [
								        mix: "good",
										fairfax: [ref: "trees"],
										water: "flint"
								]
						]
				],
				jasmine: "cute"
		]


		when:
		LocalContext localContext = new LocalContext(contextId, globalContext)
		localContext.addAll(localContextMap)


		then:
		localContext.get("config").preston.chex.fairfax == "lots of trees"
		localContext.get("vpcId") == "123"
		localContext.get("subnetId") == "abc"
		localContext.get("baz") == "Local Baz"

		localContext.get("NOT THERE") == null

		localContext.get("foo") == "a global hello"
		localContext.get("bar") == "a global world"

	}




	def "test that references work within the local context within a List" () {

		given:
		String contextId = "nodeA"

		Map<String, Object> globalContextMap = [
				foo: "a global hello",
				bar: "a global world",
				baz: "Global Baz",
				son: "Son on a global level",
				trees: "lots of trees"
		]
		GlobalContext globalContext = new GlobalContext(this.awsCredentials, globalContextMap)

		Map<String, Object> localContextMap = [
				vpcId: "123",
				subnetId: "abc",
				baz: "Local Baz",
				config: [
						aidan: "cool",
						preston: [
								trail: "rider",
								chex: [
										mix: "good",
										fairfax: ["jazz", [ref: "bar"], "hip", 0],
										water: "flint"
								]
						]
				],
				jasmine: "cute"
		]


		when:
		LocalContext localContext = new LocalContext(contextId, globalContext)
		localContext.addAll(localContextMap)


		then:
		localContext.get("config").preston.chex.fairfax[1] == "a global world"
		localContext.get("config").preston.chex.fairfax.size() == 4

	}




	def "test that local variables can be accessed from another node" () {

		given:
		String contextIdA = "nodeA"
		String contextIdB = "nodeB"

		Map<String, Object> globalContextMap = [
				foo: "a global hello",
				bar: "a global world",
				baz: "Global Baz",
				son: "Son on a global level",
				trees: "lots of trees"
		]
		GlobalContext globalContext = new GlobalContext(this.awsCredentials, globalContextMap)

		Map<String, Object> localContextMapA = [
				vpcId: "123",
				subnetId: "abc",
				baz: "Local Baz",
				config: [
						aidan: "cool",
						preston: [
								trail: "rider",
								chex: [
										mix: "good",
										fairfax: [ref: "trees"],
										water: "flint"
								]
						]
				],
				mySon: [ref: "nodeB:aidan"]
		]

		Map<String, Object> localContextMapB = [
				vpcId: "123",
				subnetId: "abc",
				baz: "Local Baz",
				aidan: "Aidan Moore"
		]

		when:
		LocalContext localContextA = new LocalContext(contextIdA, globalContext)
		localContextA.addAll(localContextMapA)

		LocalContext localContextB = new LocalContext(contextIdB, globalContext)
		localContextB.addAll(localContextMapB)


		then:
		localContextA.get("mySon") == "Aidan Moore"

	}


	def "test that local variables can be accessed from a chain of multiple nodes" () {

		given:
		String contextIdA = "nodeA"
		String contextIdB = "nodeB"
		String contextIdC = "nodeC"

		Map<String, Object> globalContextMap = [
				foo: "a global hello",
				bar: "a global world",
				baz: "Global Baz",
				son: "Son on a global level",
				trees: "lots of trees"
		]
		GlobalContext globalContext = new GlobalContext(this.awsCredentials, globalContextMap)

		Map<String, Object> localContextMapA = [
				vpcId: "123",
				subnetId: "abc",
				baz: "Local Baz",
				config: [
						aidan: "cool",
						preston: [
								trail: "rider",
								chex: [
										mySon: [ref: "nodeB:aidan"],
										mix: "good",
										fairfax: [ref: "trees"],
										water: "flint"
								]
						]
				]
		]

		Map<String, Object> localContextMapB = [
				vpcId: "123",
				subnetId: "abc",
				baz: "Local Baz",
				aidan: [ref: "nodeC:fullName"]
		]

		Map<String, Object> localContextMapC = [
				vpcId: "123",
				subnetId: "abc",
				baz: "Local Baz",
				fullName: "Aidan Xavier Moore"
		]

		when:
		LocalContext localContextA = new LocalContext(contextIdA, globalContext)
		localContextA.addAll(localContextMapA)

		LocalContext localContextB = new LocalContext(contextIdB, globalContext)
		localContextB.addAll(localContextMapB)

		LocalContext localContextC = new LocalContext(contextIdC, globalContext)
		localContextC.addAll(localContextMapC)


		then:
		localContextA.get("config").preston.chex.mySon == "Aidan Xavier Moore"

	}






} // END LocalContextTest Class
