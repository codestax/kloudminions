package com.opkloud.kloudminions.context

import com.amazonaws.auth.AWSCredentials
import com.kloudminions.auth.AWSCredentialsFactory
import com.kloudminions.process.Process
import net.sf.cglib.core.Local
import spock.lang.Shared
import spock.lang.Specification

/**
 * Created by rashadmoore on 4/26/16.
 */
class GlobalContextSpec extends Specification {


	@Shared
	AWSCredentials awsCredentials  = AWSCredentialsFactory.getInstance(AWSCredentialsFactory.AWS, AWSCredentialsFactory.TEST)


	def setup() {
		GlobalContext.LOCAL_CONTEXT_ID_COUNTER = 0
	}

	def "create a simple GlobalContext via constructor AWSCredentials only"() {

		when:
		GlobalContext globalContext = new GlobalContext(this.awsCredentials)

		then:
		globalContext.getGlobalContextMap().size() == 0
		globalContext.getProcess() == null
		globalContext.getNumberOfLocalContexts() == 0
		GlobalContext.LOCAL_CONTEXT_ID_COUNTER == 0

	}


	def "create a simple GlobalContext via constructor AWSCredentials and globalContextMap"() {

		given:
		Map<String, Object> globalContextMap = [
		        foo: "bar",
				west: "side",
				tesla: ["power wall", "model s", "model x", "model 3"],
				ping: "pong"
		]

		when:
		GlobalContext globalContext = new GlobalContext(this.awsCredentials, globalContextMap)

		then:
		globalContext.getGlobalContextMap().size() == 4
		globalContext.getGlobalContextMap().get("foo") == "bar"
		globalContext.get("foo") == "bar"
		globalContext.get("tesla")[2] == "model x"

		globalContext.getProcess() == null
		globalContext.getNumberOfLocalContexts() == 0
		GlobalContext.LOCAL_CONTEXT_ID_COUNTER == 0
	}


	def "create a simple GlobalContext via constructor AWSCredentials, globalContextMap and Process"() {

		given:
		Map<String, Object> globalContextMap = [
				foo: "bar",
				west: "side",
				tesla: ["power wall", "model s", "model x", "model 3"],
				ping: "pong"
		]

		Map<String, Object> processSpecMap = [
												type: "sequence",
												nodes: [[
																type: "action",
																id: "nodeA",
																action: "cloud_formation_minion:deleteStack"
														]]
										]


		when:
		Process process = Process.createInstance(processSpecMap, this.awsCredentials, globalContextMap)
		GlobalContext globalContext = process.getGlobalContext()


		then:
		globalContext.getGlobalContextMap().size() == 4
		globalContext.getGlobalContextMap().get("foo") == "bar"
		globalContext.get("foo") == "bar"
		globalContext.get("tesla")[2] == "model x"

		globalContext.getProcess() != null
		globalContext.getProcess() instanceof Process

		globalContext.getNumberOfLocalContexts() == 2
		GlobalContext.LOCAL_CONTEXT_ID_COUNTER == 1
	}



	def "create a simple GlobalContext via factory method AWSCredentials, globalContextMap"() {

		given:
		Map<String, Object> globalContextMap = [
				foo: "bar",
				west: "side",
				tesla: ["power wall", "model s", "model x", "model 3"],
				ping: "pong"
		]

		Map<String, Object> processSpecMap = [
											  	type: "sequence",
											  	nodes: [[
														type: "action",
														id: "nodeA",
														action: "cloud_formation_minion:deleteStack"
											  	]]
											]


		when:
		Process process = Process.createInstance(processSpecMap, this.awsCredentials, globalContextMap)
		GlobalContext globalContext = process.getGlobalContext()


		then:
		globalContext.getGlobalContextMap().size() == 4
		globalContext.getGlobalContextMap().get("foo") == "bar"
		globalContext.get("foo") == "bar"
		globalContext.get("tesla")[2] == "model x"

		globalContext.getProcess() != null
		globalContext.getProcess() instanceof Process

		globalContext.getNumberOfLocalContexts() == 2
		GlobalContext.LOCAL_CONTEXT_ID_COUNTER == 1
	}



	def "ensure local context support - single local context"() {

		given:
		Map<String, Object> globalContextMap = [
				foo: "bar",
				west: "side",
				tesla: ["power wall", "model s", "model x", "model 3"],
				ping: "pong"
		]

		Map<String, Object> processSpecMap = [
				type: "sequence",
				nodes: [[
								type: "action",
								id: "nodeA",
								action: "cloud_formation_minion:deleteStack"
						]]
		]

		Map<String, Object> localContextMapB = [
		        car: "wheel",
				plane: "wing"
		]


		Process process = Process.createInstance(processSpecMap, this.awsCredentials, globalContextMap)
		GlobalContext globalContext = process.getGlobalContext()

		String contextIdB = "nodeB"
		LocalContext localContextB = new LocalContext(contextIdB, localContextMapB, globalContext)


		when:
		globalContext.addLocalContext(contextIdB, localContextB)
		LocalContext resultLocalContextB = globalContext.getLocalContext(contextIdB)


		then:
		globalContext.getGlobalContextMap().size() == 4
		globalContext.getGlobalContextMap().get("foo") == "bar"
		globalContext.get("foo") == "bar"
		globalContext.get("tesla")[2] == "model x"

		globalContext.getProcess() != null
		globalContext.getProcess() instanceof Process

		globalContext.getNumberOfLocalContexts() == 3
		GlobalContext.LOCAL_CONTEXT_ID_COUNTER == 1

		globalContext.get("nodeB:car") == "wheel"
		globalContext.get("nodeB:plane") == "wing"

		resultLocalContextB.get("car") == "wheel"


	}




} // END GlobalContextSpec Class
