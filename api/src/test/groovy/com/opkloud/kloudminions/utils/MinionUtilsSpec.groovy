package com.opkloud.kloudminions.utils

import spock.lang.Specification

class MinionUtilsSpec extends Specification {


	def "get the current directory" () {

		when:
		File currDir = MinionUtils.getCurrentDirectory()
		String fileName = currDir.path

		then:
		fileName.equals(".")
	}



	def "readJsonFromFile(String fileName) - read a normal file as json"() {

		given:
		String filePath = "./src/test/resources/test_files/minionUtilsTest_json.json"


		when:
		Map<String, Object> jsonObj = MinionUtils.readJsonFromFile(filePath)


		then:
		jsonObj.foo.equals("bar")
		jsonObj.bat.equals(1)

	}



	def "readJsonFromFile(File file) - read a file object as json"() {

		given:
		String filePath = "./src/test/resources/test_files/minionUtilsTest_json.json"
		File file = new File(filePath)

		when:
		Map<String, Object> jsonObj = MinionUtils.readJsonFromFile(file)


		then:
		jsonObj.foo.equals("bar")
		jsonObj.bat.equals(1)

	}


} // END MinionUtilsSpec Class
