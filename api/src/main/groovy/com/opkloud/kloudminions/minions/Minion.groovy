package com.opkloud.kloudminions.minions

import com.opkloud.kloudminions.context.Context
import com.opkloud.kloudminions.executable.Executable

interface Minion extends Executable {

	void execute(Context context)

	
}
