package com.opkloud.kloudminions.minions.aws

import com.opkloud.kloudminions.context.Context
import com.opkloud.kloudminions.minions.AbstractMinion
import com.opkloud.kloudminions.minions.KloudMinion
import groovy.transform.InheritConstructors
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@InheritConstructors
@KloudMinion(name="describer_rlm")
class DescriberMinion extends AbstractMinion {


	private static final Logger log = LoggerFactory.getLogger(DescriberMinion.class)

	DescriberMinion(Context context) {
		super(context)
	}

	@Override
	void execute() {

	}
}
