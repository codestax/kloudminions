package com.opkloud.kloudminions.minions.aws

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.CreateKeyPairRequest
import com.amazonaws.services.ec2.model.CreateKeyPairResult
import com.amazonaws.services.ec2.model.DeleteKeyPairRequest
import com.amazonaws.services.ec2.model.KeyPair
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.S3Object
import com.amazonaws.services.s3.model.S3ObjectInputStream
import com.opkloud.kloudminions.context.Context
import com.opkloud.kloudminions.minions.AbstractMinion
import com.opkloud.kloudminions.minions.KloudMinion
import com.opkloud.kloudminions.exceptions.MinionException
import groovy.json.JsonSlurper
import groovy.transform.InheritConstructors
import org.apache.commons.io.IOUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.nio.charset.Charset

@InheritConstructors
@KloudMinion(name="credentials_minion")
class CredentialsMinion extends AbstractMinion {

	private AWSCredentials awsCredentials = this.context.getAWSCredentials()
	private static final Logger log = LoggerFactory.getLogger(CredentialsMinion.class)

	CredentialsMinion(Context context) {
		super(context)
	}

	@Override
	void execute() {

	}

	void loadCredentialsFromS3(){
		AmazonS3 amazonS3 = new AmazonS3Client(awsCredentials)
		amazonS3.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

		if(!amazonS3.doesObjectExist(this.context.get("bucketName"),this.context.get("fileName"))){
			throw new MinionException("Credentials file does not exist in S3!")
		}

		log.info("Retrieving credentials from s3...")

		S3Object s3Object = amazonS3.getObject(this.context.get("bucketName"),this.context.get("fileName"))

		Map<String, Object> credentialsMap = getCredentialsMap(s3Object.getObjectContent())

		log.info("Got credentials from s3...")

		this.context.addAll(credentialsMap)
	}

	private void createKeypair(){
		AmazonEC2 ec2Client = new AmazonEC2Client(awsCredentials)
		ec2Client.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

		AmazonS3 amazonS3 = new AmazonS3Client(awsCredentials)
		amazonS3.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

		log.info("Creating keypair with the name " + this.context.get("keyName") + "...")

		if(keyPairExist(ec2Client.describeKeyPairs().keyPairs)){

			log.info("Keypair with the name " + this.context.get("keyName") + " already exists. Moving on.")
		}else{

			log.info("Creating keypair with the name " + this.context.get("keyName") + "...")

			CreateKeyPairResult keyPairResult = ec2Client.createKeyPair(new CreateKeyPairRequest()
					.withKeyName(this.context.get("keyName")))

			KeyPair keyPair = keyPairResult.getKeyPair()
			InputStream keypairIs = IOUtils.toInputStream(keyPair.getKeyMaterial(),"UTF-8")

			String keyName = this.context.get("keyName") + ".pem"

			amazonS3.putObject(this.context.get("bucketName"),keyName,keypairIs,null)

			log.info("Keypair with the name " + this.context.get("keyName") + "has been created. The private key has been stored in " + this.context.get("bucketName"))

		}
	}

	private boolean keyPairExist(List<KeyPair> keyPairs){

		boolean result = false
		keyPairs.each { keyPair ->
			if(keyPair.keyName.equalsIgnoreCase(this.context.get("keyName"))){
				result = true
			}
		}

		return result

	}

	private void deleteKeypair(){
		AmazonEC2 ec2Client = new AmazonEC2Client(awsCredentials)
		ec2Client.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

		log.info("Deleting keypair with the name " + this.context.get("keyName") + "...")

		ec2Client.deleteKeyPair(new DeleteKeyPairRequest().withKeyName(this.context.get("keyName")))

		log.info("Keypair with the name " + this.context.get("keyName") + "has been deleted.")
	}

	private Map<String, Object> getCredentialsMap(S3ObjectInputStream s3ObjectInputStream) {
		StringWriter writer = new StringWriter()
		IOUtils.copy(s3ObjectInputStream, writer, Charset.defaultCharset())
		String credentialsContent = writer.toString()

		JsonSlurper jsonSlurper = new JsonSlurper();
		Map<String, Object> credentialsMap = jsonSlurper.parseText(credentialsContent)

		return credentialsMap
	}
}
