package com.opkloud.kloudminions.minions

import com.opkloud.kloudminions.context.Context
import groovy.transform.InheritConstructors

@InheritConstructors
abstract class AbstractMinion implements Minion {

	protected Context context

	
	/**
	 * Prevents Minions from being created via the empty constructor
	 */
	AbstractMinion() {
		throw new IllegalArgumentException("ERROR: Cannot instantiate a Minion with the empty constructor")
	}

	AbstractMinion(Context context) {
		
		if (context == null) {
			throw new IllegalArgumentException("context is null")
		}
		
		this.context = context
	}

	Context getContext() {
		return context
	}

	void setContext(Context context) {
		this.context = context
	}

	void execute(Context context) {
		this.context = context
		this.execute()
	}

} // END AbstractMinion Class
