package com.opkloud.kloudminions.minions.aws

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.rds.AmazonRDS
import com.amazonaws.services.rds.AmazonRDSClient
import com.amazonaws.services.rds.model.*
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonParser
import com.opkloud.kloudminions.context.Context
import com.opkloud.kloudminions.minions.AbstractMinion
import com.opkloud.kloudminions.minions.KloudMinion
import com.opkloud.kloudminions.exceptions.MinionException
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@KloudMinion(name="rds_minion")
class RDSMinion extends AbstractMinion {

    private AWSCredentials awsCredentials = this.context.getAWSCredentials()
    private  Map<String, Parameter> parameterMap = new HashMap<>()
    private Gson gson = new Gson()

    private static final Logger log = LoggerFactory.getLogger(RDSMinion.class)


    RDSMinion(Context context) {
        super(context)
    }


    @Override
    void execute() {
        // TODO Auto-generated method stub
    }

    void createDbParameterGroup() {
        AmazonRDS rdsClient = new AmazonRDSClient(awsCredentials)
        rdsClient.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

        log.info("Creating ParameterGroup named " + this.context.get("parameter_group_name") + "...")

        try {
             rdsClient.createDBParameterGroup(new CreateDBParameterGroupRequest()
                    .withDBParameterGroupName(this.context.get("parameter_group_name"))
                    .withDescription(this.context.get("parameter_group_description"))
                    .withDBParameterGroupFamily(this.context.get("parameter_group_family")))
            log.info("ParameterGroup named " + this.context.get("parameter_group_name") + " has been created!")
        }
        catch (DBParameterGroupAlreadyExistsException exception) {
            log.info("ParameterGroup named " + this.context.get("parameter_group_name") + " already exists! Proceeding to modify parameters")
        }
        catch (DBParameterGroupQuotaExceededException exception) {
            throw new MinionException(exception.message, exception)
        }

        modifyParametersInParameterGroup(this.context.get("parameter_group_name"))

        context.add("parameterGroupName", this.context.get("parameter_group_name"))
    }

    void createDbOptionGroup(){
        AmazonRDS rdsClient = new AmazonRDSClient(awsCredentials)
        rdsClient.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

        log.info("Creating OptionGroup named " + this.context.get("option_group_name") + "...")

        OptionGroup optionGroup = null

        try {
            optionGroup = rdsClient.createOptionGroup(new CreateOptionGroupRequest()
                    .withEngineName(this.context.get("engine_name"))
                    .withMajorEngineVersion(this.context.get("engine_version"))
                    .withOptionGroupDescription(this.context.get("option_group_description"))
                    .withOptionGroupName(this.context.get("option_group_name")))
            log.info("OptionGroup named " + this.context.get("option_group_name") + " has been created!")
        }
        catch (OptionGroupAlreadyExistsException exception) {
            log.info("OptionGroup named " + this.context.get("option_group_name") + " already exists! Proceeding to modify options")
        }
        catch (OptionGroupQuotaExceededException exception) {
            throw new MinionException(exception.message, exception)
        }

        modifyOptionsInOptionGroup()

    }

    void modifyOptionsInOptionGroup() {
        AmazonRDS rdsClient = new AmazonRDSClient(awsCredentials)
        rdsClient.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

        System.out.println("******* " + getOptionList().get(0).getClass().name)

        log.info("Modifying options in " + this.context.get("option_group_name") + "...")
        rdsClient.modifyOptionGroup(new ModifyOptionGroupRequest().withOptionGroupName(this.context.get("option_group_name")).withApplyImmediately(this.context.get("apply_immediately")).withOptionsToInclude(getOptionList()))
        log.info("Options in " + this.context.get("option_group_name") + " have been modified!")
    }

    void modifyParametersInParameterGroup(String parameterGroupName) {
        AmazonRDS rdsClient = new AmazonRDSClient(awsCredentials)
        rdsClient.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

        log.info("Modifying parameters in " + parameterGroupName + "...")
        rdsClient.modifyDBParameterGroup(new ModifyDBParameterGroupRequest()
                .withDBParameterGroupName(parameterGroupName).withParameters(getParameterList()))
        log.info("Parameters in " + parameterGroupName + " have been modified!")

    }

    void getLatestSnapshot() {
        log.info("Retrieving last RDS snapshot...")
        AmazonRDS rdsClient = new AmazonRDSClient(awsCredentials)
        rdsClient.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

        DescribeDBSnapshotsResult results = rdsClient.describeDBSnapshots(new DescribeDBSnapshotsRequest()
                .withDBInstanceIdentifier(this.context.get("db_instance_id")))

        List<DBSnapshot> snapshots = results.getDBSnapshots()

        snapshots.sort { it.snapshotCreateTime }
        snapshots.reverse(true)

        if (snapshots.empty) {
            context.add("snapshotName", "")
            log.info("No snapshots exist. Proceeding to next process.")
        } else {
            context.add("snapshotName", snapshots.get(0).getDBSnapshotIdentifier())
            log.info("RDS snapshot " + snapshots.get(0).getDBSnapshotIdentifier() + " was retrieved")
        }

    }

    private List<Parameter> getParameterList() {
        //TODO: If there are more than 20 parameters split the call. AWS restricts each requests to 20 parameters
        Map<String, Object> parametersFromContext = this.context.get("parameters")
        List<Parameter> parameters = []

        parametersFromContext.each { key, value ->
            if(isParameterModifiable("${key}")){
                parameters.add(new Parameter().withParameterName("${key}")
                        .withParameterValue("${value}").withApplyMethod(getApplyMethod("${key}")))
            }
        }

        return parameters
    }

    private List<OptionConfiguration> getOptionList(){

        String optionString = gson.toJson(this.context.get("options"))

        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse(optionString).getAsJsonArray();

        List<OptionConfiguration> lst =  new ArrayList<>();
        for(final JsonElement json: array){
            OptionConfiguration entity = gson.fromJson(json, OptionConfiguration.class);
            lst.add(entity);
        }




        return lst

    }

    private String getApplyMethod(String name) {
        populateParameterMap()
        return parameterMap.get(name).getApplyMethod()
    }

    private boolean isParameterModifiable(String name) {
        populateParameterMap()
        return parameterMap.get(name).isModifiable()
    }

    private void populateParameterMap(){
        AmazonRDS rdsClient = new AmazonRDSClient(awsCredentials)
        rdsClient.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

        if(parameterMap.size() == 0)
        {
            DescribeDBParametersResult results = rdsClient.describeDBParameters(new DescribeDBParametersRequest()
                    .withDBParameterGroupName(this.context.get("parameter_group_name")))
            results.getParameters().forEach{
                parameterMap.put(it.parameterName,it)
            }
        }

    }


}
