package com.opkloud.kloudminions.minions.aws

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.autoscaling.AmazonAutoScaling
import com.amazonaws.services.autoscaling.AmazonAutoScalingClient
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsRequest
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsResult
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.CreateRouteRequest
import com.amazonaws.services.route53.AmazonRoute53
import com.amazonaws.services.route53.AmazonRoute53Client
import com.amazonaws.services.route53.model.AssociateVPCWithHostedZoneRequest
import com.amazonaws.services.route53.model.GetHostedZoneRequest
import com.amazonaws.services.route53.model.GetHostedZoneResult
import com.amazonaws.services.route53.model.VPC
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonParser
import com.opkloud.kloudminions.context.Context
import com.opkloud.kloudminions.minions.AbstractMinion
import com.opkloud.kloudminions.minions.KloudMinion
import groovy.transform.InheritConstructors
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@InheritConstructors
@KloudMinion(name="vpc_minion")
class VPCMinion extends AbstractMinion {

	private AWSCredentials awsCredentials = this.context.getAWSCredentials()
	private static final Logger log = LoggerFactory.getLogger(VPCMinion.class)
	private Gson gson = new Gson()

	VPCMinion(Context context) {
		super(context)
	}

	@Override
	void execute() {

	}

	void associateVPCWithHostedZone(){
		AmazonRoute53 route53 = new AmazonRoute53Client(awsCredentials)
		route53.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

		if(!vpcAssociatedWithZone(route53)) {

			log.info("Associating VPC with hostedzone ...")
			route53.associateVPCWithHostedZone(new AssociateVPCWithHostedZoneRequest()
					.withHostedZoneId(this.context.get("hostedZoneId"))
					.withVPC(new VPC()
					.withVPCId(this.context.get("vpcId"))
					.withVPCRegion(this.context.get("aws_region"))))

			log.info("Associating VPC with hostedzone complete!")
		}

	}

	private boolean vpcAssociatedWithZone(AmazonRoute53Client route53) {
		GetHostedZoneResult hostedZoneResult = route53.getHostedZone(new GetHostedZoneRequest().withId(this.context.get("hostedZoneId")))

		List<VPC> vpcs = hostedZoneResult.getVPCs()

		boolean result = false

		for (VPC vpc : vpcs) {
			if (vpc.getVPCId().equalsIgnoreCase(this.context.get("vpcId"))) {
				result = true
				break;
			}
		}

		return result
	}

	private void createRoutes(){
		AmazonEC2 ec2Client = new AmazonEC2Client(awsCredentials)
		ec2Client.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

		log.info("Creating routes ...")
		Map<String, Object> publicRoute = this.context.get("publicRoute")

		CreateRouteRequest req = new CreateRouteRequest()
		publicRoute.each { key, value ->
			if("${key}".equalsIgnoreCase("destinationCidrBlock")){
				req.withDestinationCidrBlock("${value}")
			}
			else{
				req.withRouteTableId("${value}")
			}
		}

		req.withInstanceId(getInstanceId(this.context.get("autoScaleGroup")))
		ec2Client.createRoute(req)

		Map<String, Object> privateRoute = this.context.get("privateRoute")

		req = new CreateRouteRequest()
		privateRoute.each { key, value ->

			if("${key}".equalsIgnoreCase("destinationCidrBlock")){
				req.withDestinationCidrBlock("${value}")
			}
			else{
				req.withRouteTableId("${value}")
			}
		}

		req.withInstanceId(getInstanceId(this.context.get("autoScaleGroup")))
		ec2Client.createRoute(req)

		log.info("Routes created ...")

		}

	private List<CreateRouteRequest> getRouteRequests(){
		String optionString = gson.toJson(this.context.get("routes"))
		System.out.println(this.context.get("routes"))

		JsonParser parser = new JsonParser();
		JsonArray array = parser.parse(optionString).getAsJsonArray();

		List<CreateRouteRequest> lst =  new ArrayList<>();
		for(final JsonElement json: array){
			CreateRouteRequest entity = gson.fromJson(json, CreateRouteRequest.class);
			lst.add(entity);
		}
		return lst
	}

	private String getInstanceId(String autoScalingGroupName){
		AmazonAutoScaling autoScalingClient = new AmazonAutoScalingClient(awsCredentials)
		autoScalingClient.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

		DescribeAutoScalingGroupsResult result = autoScalingClient.describeAutoScalingGroups(
				new DescribeAutoScalingGroupsRequest().withAutoScalingGroupNames(autoScalingGroupName))

		return result.getAutoScalingGroups().get(0).getInstances().get(0).instanceId
	}

}
