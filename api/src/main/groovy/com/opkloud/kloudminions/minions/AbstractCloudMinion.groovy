package com.opkloud.kloudminions.minions

import groovy.transform.InheritConstructors

import com.opkloud.kloudminions.context.GlobalContext

@InheritConstructors
abstract class AbstractCloudMinion extends AbstractMinion {

	AbstractCloudMinion(GlobalContext context) {

		super(context)
	}

	
} // END AbstractCloudMinion Class
