package com.opkloud.kloudminions.minions.aws

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.codedeploy.AmazonCodeDeploy
import com.amazonaws.services.codedeploy.AmazonCodeDeployClient
import com.amazonaws.services.codedeploy.model.*
import com.opkloud.kloudminions.context.Context
import com.opkloud.kloudminions.minions.AbstractMinion
import com.opkloud.kloudminions.minions.KloudMinion
import groovy.transform.InheritConstructors
import org.apache.commons.compress.archivers.ArchiveOutputStream
import org.apache.commons.compress.archivers.tar.TarArchiveEntry
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream
import org.apache.commons.compress.utils.IOUtils
import org.apache.commons.io.FileUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.util.zip.GZIPOutputStream

@InheritConstructors
@KloudMinion(name="deployer_minion")
class DeployerMinion extends AbstractMinion {

    private static String CURRENT_DIR = "." + File.separator
    private static String MAVEN_TARGET_DIR = CURRENT_DIR + "target"
    private static String GRADLE_BUILD_DIR = CURRENT_DIR + "build" + File.separator + "libs"
    private static String KM_INFRA_DIR = CURRENT_DIR + "build" + File.separator + "infrastructure"
    private static String PACKAGED_DEPLOYMENT = CURRENT_DIR + "build" + File.separator


	private static final Logger log = LoggerFactory.getLogger(DeployerMinion.class)


    DeployerMinion(Context context) {
        super(context)
    }


    @Override
    void execute() {
        // TODO Auto-generated method stub
    }


    void createDeployment() {

        log.info("Creating Deployment")

        if (Files.exists(Paths.get(GRADLE_BUILD_DIR))) {
            copyJavaDeploymentsToInfraDir(gatherJavaDeployments(GRADLE_BUILD_DIR))
        } else if (Files.exists(Paths.get(MAVEN_TARGET_DIR))) {
            copyJavaDeploymentsToInfraDir(gatherJavaDeployments(MAVEN_TARGET_DIR))
        } else {
            copySourceFilesToInfraDir()
        }

        Map<String, Object> parametersFromContext = this.context.get("deployments")

        parametersFromContext.each { key, value ->

            def pathToDeployment = createTarFile(CURRENT_DIR + "${value}", PACKAGED_DEPLOYMENT + "${key}" + ".tar.gz")

            this.context.add("${key}" +"-path", [pathToDeployment])
            this.context.add("${key}"+"-filename", "${key}" + ".tar.gz")

        }

        log.info("Finished Creating Deployment")

    }

    void createKpDeployment(){

        Map<String, Object> parametersFromContext = this.context.get("deployments")

        parametersFromContext.each { key, value ->

            def pathToDeployment = createTarFile(CURRENT_DIR + "${value}", PACKAGED_DEPLOYMENT + "${key}" + ".tar.gz")

            this.context.add("${key}" +"-path", [pathToDeployment])
            this.context.add("${key}"+"-filename", "${key}" + ".tar.gz")

        }


    }

    private def createTarFile(String dirToArchive, String outputFilePath) throws Exception {

        log.info("**** Archiving " + dirToArchive)

        if (!Files.exists(Paths.get(PACKAGED_DEPLOYMENT))) {
            Files.createDirectory(Paths.get(PACKAGED_DEPLOYMENT))
        }
        Path tarFilePath = Paths.get(outputFilePath)
        File tarFile = tarFilePath.toFile()
        OutputStream out = new FileOutputStream(tarFile);

        TarArchiveOutputStream aos = new TarArchiveOutputStream(
                new GZIPOutputStream(new BufferedOutputStream(out)))

        // TAR has an 8 gig file limit by default, this gets around that
        aos.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_STAR);

        // TAR originally didn't support long file names, this enables the support for it
        aos.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);

        for (File file : Paths.get(dirToArchive).toFile().listFiles()) {

            addFilesToCompression(aos, file, ".", dirToArchive.substring(2));
        }
        aos.finish();
        aos.close();
        out.close();


        return tarFilePath.toRealPath()
    }

    private static void addFilesToCompression(ArchiveOutputStream taos, File file, String dir, String parentDir) throws IOException {

        TarArchiveEntry entry = new TarArchiveEntry(file, dir + File.separator + file.getName())
        entry.setSize(file.length())
        taos.putArchiveEntry(entry)

        if (file.isFile()) {
            log.debug(file.getCanonicalPath().substring(file.getCanonicalPath().indexOf(parentDir) + parentDir.length() + 1))
            // Add the file to the archive
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
            IOUtils.copy(bis, taos);
            taos.closeArchiveEntry();
            bis.close();

        } else if (file.isDirectory()) {
            // close the archive entry
            taos.closeArchiveEntry();
            // go through all the files in the directory and using recursion, add them to the archive

            for (File childFile : file.listFiles()) {
                log.debug(file.getCanonicalPath().substring(file.getCanonicalPath().indexOf(parentDir) + parentDir.length() + 1))
                addFilesToCompression(taos, childFile, file.getCanonicalPath().substring(file.getCanonicalPath().indexOf(parentDir) + parentDir.length() + 1), parentDir);
            }
        }

    }

     void pushDeployment() {

         AmazonCodeDeployClient codeDeployClient = getCodeDeployClient()

        def existingApplication = codeDeployClient.listApplications()

        if (existingApplication.applications.contains(this.context.get("applicationName"))) {

            log.info("Application already exists in code deploy. Pushing deployment")

            codeDeployClient.deleteDeploymentGroup(getDeleteDeploymentGroupReq())
            codeDeployClient.deleteApplication(getDeleteAppRequest())
        }

        codeDeployClient.createApplication(getApplicationRequest())
        codeDeployClient.createDeploymentGroup(getCodeDeploymentGroupRequest())
        CreateDeploymentResult deploymentResult = codeDeployClient.createDeployment(createDeploymentRequest())

            String deploymentId = deploymentResult.getDeploymentId()
         this.waitUntil(this.&isDeploymentComplete, true, deploymentId)

        log.info("Application: " + this.context.get("applicationName") + "has been pushed to cloud!")

    }

    private void waitUntil(closure, targetValue, String deploymentId, long pollPeriodLength = 5000, long timeOutCycles = 200, boolean verbose = true) {

        for (int i = 0; i < timeOutCycles; ++i) {

            if (closure(deploymentId) == targetValue) {
                return
            }

            if (verbose) {
                log.debug("Waiting for completion of the task.")
            }
            Thread.sleep(pollPeriodLength)
        }
    }

    private boolean isDeploymentComplete(String deploymentId){
       String status = getDeploymentStatus(deploymentId)

        log.info("Deploying Application... " + this.context.get("applicationName") + " - Current Status: " + status + " - Target Status: " + DeploymentStatus.Succeeded.toString())

        if(status.equalsIgnoreCase(DeploymentStatus.Failed.toString()) || status.equalsIgnoreCase(DeploymentStatus.Succeeded.toString()) || status.equalsIgnoreCase(DeploymentStatus.Stopped.toString())){
            return true
        }
        else{
            return false;
        }
    }

    private String getDeploymentStatus(String deploymentId) {
        AmazonCodeDeployClient codeDeployClient = getCodeDeployClient()

        GetDeploymentResult deploymentResult = codeDeployClient.getDeployment(new GetDeploymentRequest()
                .withDeploymentId(deploymentId))

        DeploymentInfo deploymentInfo = deploymentResult.getDeploymentInfo()

       return deploymentInfo.getStatus()
    }

    private DeleteDeploymentGroupRequest getDeleteDeploymentGroupReq() {
        DeleteDeploymentGroupRequest ddg = new DeleteDeploymentGroupRequest()
                .withApplicationName(this.context.get("applicationName"))
                .withDeploymentGroupName(this.context.get("deploymentGroupName"))
        return ddg
    }

    private DeleteApplicationRequest getDeleteAppRequest() {
        DeleteApplicationRequest dar = new DeleteApplicationRequest()
                .withApplicationName(this.context.get("applicationName"))
        return dar
    }

    private CreateApplicationRequest getApplicationRequest() {
        CreateApplicationRequest applicationRequest = new CreateApplicationRequest().withApplicationName(this.context.get("applicationName"))
        return applicationRequest
    }

    private CreateDeploymentGroupRequest getCodeDeploymentGroupRequest() {
        CreateDeploymentGroupRequest deploymentGroupRequest = new CreateDeploymentGroupRequest()
                .withApplicationName(this.context.get("applicationName"))
                .withAutoScalingGroups(this.context.get("autoScaleGroup"))
                .withDeploymentGroupName(this.context.get("deploymentGroupName"))
                .withServiceRoleArn(this.context.get("codeDeployRoleName"))
        return deploymentGroupRequest
    }

    private AmazonCodeDeployClient getCodeDeployClient() {
        AWSCredentials awsCredentials = this.context.getAWSCredentials()
        AmazonCodeDeploy codeDeployClient = new AmazonCodeDeployClient(awsCredentials);
        codeDeployClient.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))
        return codeDeployClient
    }

    private CreateDeploymentRequest createDeploymentRequest() {
        CreateDeploymentRequest deploymentRequest = new CreateDeploymentRequest()
                .withDeploymentGroupName(this.context.get("deploymentGroupName"))
                .withApplicationName(this.context.get("applicationName"))
                .withRevision(new RevisionLocation()
                .withS3Location(new S3Location().withBucket(this.context.get("deploymentS3Location"))
                .withKey(this.context.get("deployment_file_name"))
                .withBundleType(BundleType.Tgz))
                .withRevisionType(RevisionLocationType.S3))

       return deploymentRequest
    }

    private def gatherJavaDeployments(String pathToSearch) {

        Path[] deployments = Files.walk(Paths.get(pathToSearch), 1).filter { p ->
            p.getFileName()
                    .toString()
                    .endsWith(".jar") || p.getFileName().toString().endsWith(".war")
        }
        .collect()
        return deployments
    }

    private def copyJavaDeploymentsToInfraDir(Path[] deployments) {
        Paths.get(KM_INFRA_DIR + File.separator + "deployments").toFile().mkdir()

        deployments.each { deployment ->
            Files.copy(Paths.get(deployment.toAbsolutePath().toString()),
                    Paths.get(KM_INFRA_DIR + File.separator + "deployments" + File.separator +
                            deployment.fileName.toString()), StandardCopyOption.REPLACE_EXISTING)
        }
    }

    private def copySourceFilesToInfraDir() {
        Paths.get(KM_INFRA_DIR + File.separator + "deployments").toFile().mkdir()

        FileUtils.copyDirectory(Paths.get(".").toFile(), Paths.get(KM_INFRA_DIR + File.separator + "deployments").toFile(), new FileFilter() {

            @Override
            boolean accept(File pathname) {

                return !(pathname.toPath().normalize().startsWith("build"));

            }
        });
    }

} // END DeployerMinion Class