package com.opkloud.kloudminions.minions.aws

import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.cloudformation.AmazonCloudFormationClient
import com.amazonaws.services.cloudformation.model.*
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.GetObjectRequest
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.services.s3.model.S3Object
import com.opkloud.kloudminions.context.Context
import com.opkloud.kloudminions.minions.AbstractMinion
import com.opkloud.kloudminions.minions.KloudMinion
import com.opkloud.kloudminions.exceptions.MinionException
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@KloudMinion(name = "cloud_formation_minion")
class CloudFormationMinion extends AbstractMinion {


	private static final Logger log = LoggerFactory.getLogger(CloudFormationMinion.class)


	CloudFormationMinion(Context context) {
		super(context)
	}


	@Override
	void execute() {
		// TODO Auto-generated method stub
	}

	/**
	 * Action
	 */
	void createStack() {

		log.debug("Start: CloudFormation Minion --> createStack")

		String stackName = this.context.get("aws_stack_name")
		if (this.doesStackExist() == true) {
			log.info("Stack " + stackName + " already exists; we cannot create this new stack without deleting the previous stack")
			return
		}

		Map<String, Object> cfParametersFromContext = this.context.get("cf_parameters")
		def cfParameters = []

		cfParametersFromContext.each { key, value ->
			cfParameters.add(new Parameter().withParameterKey("${key}").withParameterValue("${value}"))
		}

		AWSCredentials awsCredentials = this.context.getAWSCredentials()
		AmazonCloudFormationClient cloudFormationClient = new AmazonCloudFormationClient(awsCredentials)
		cloudFormationClient.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

		def createRequest = new CreateStackRequest()
		createRequest.setStackName(stackName)

		String cf_script_filename = this.context.get("cf_script_filename")
//		if(cf_script_filename.contains(":")){
//			String [] fileParts = cf_script_filename.split(":");
//			cf_script_filename = "./build/tmp/" +fileParts[0] + "/cloudformation/" + fileParts[1]
//		}
		File cf_file = new File(cf_script_filename)
		String cf_template = cf_file.getText()
		createRequest.setTemplateBody(cf_template)
		createRequest.setCapabilities(Arrays.asList("CAPABILITY_IAM")) // TODO: Remove hardcoded string?
		createRequest.setParameters(cfParameters)
		System.println(cfParameters)
		createRequest.withDisableRollback(true)

		CreateStackResult createStackResult = cloudFormationClient.createStack(createRequest)

		this.waitUntil(this.&stackCompleted, true)
		log.info("Stack " + stackName + " Created!")

		log.debug("End: CloudFormation Minion --> createStack")
	}


	/**
	 * Action
	 */
	void deleteStack() {

		log.debug("Start: CloudFormation Minion --> deleteStack")

		String stackName = this.context.get("aws_stack_name")
		if (this.doesStackExist() == false) {
			log.info("Stack " + stackName + " does not exist, so there is no need to delete the stack")
			return
		}

		AWSCredentials awsCredentials = this.context.getAWSCredentials()
		AmazonCloudFormationClient cloudFormationClient = new AmazonCloudFormationClient(awsCredentials)
		cloudFormationClient.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

		DeleteStackRequest deleteStackRequest = new DeleteStackRequest()
		deleteStackRequest.setStackName(stackName)

		cloudFormationClient.deleteStack(deleteStackRequest)

		this.waitUntil(this.&doesStackExist, false)
		log.info("Stack " + stackName + " Deleted!")

		log.debug("End: CloudFormation Minion --> deleteStack")
	}

	/**
	 * Action
	 */
	void deleteCreateStack() {

		log.debug("Start: CloudFormation Minion --> deleteCreateStack")

		this.deleteStack()
		this.createStack()

		log.debug("End: CloudFormation Minion --> deleteCreateStack")
	}


	private void waitUntil(closure, targetValue, long pollPeriodLength = 5000, long timeOutCycles = 200, boolean verbose = true) {

		for (int i = 0; i < timeOutCycles; ++i) {

			if (closure() == targetValue) {
				return
			}

			if (verbose) {
				log.debug("Waiting for completion of the task.")
			}
			Thread.sleep(pollPeriodLength)
		}
	}


	private DescribeStacksResult describeStack() {


		AWSCredentials awsCredentials = this.context.getAWSCredentials()
		AmazonCloudFormationClient cloudFormationClient = new AmazonCloudFormationClient(awsCredentials)
		cloudFormationClient.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

		String stackName = this.context.get("aws_stack_name")
		DescribeStacksRequest describeStackRequest = new DescribeStacksRequest()
		describeStackRequest.setStackName(stackName)

		DescribeStacksResult describeStacksResult = cloudFormationClient.describeStacks(describeStackRequest)

		return describeStacksResult
	}

	/*

	 */

	private boolean doesStackExist() {


		DescribeStacksResult describeStacksResult = null

		try {
			describeStacksResult = this.describeStack()
		} catch (AmazonServiceException ase) {
			return false
		}

		List<com.amazonaws.services.cloudformation.model.Stack> stacks = describeStacksResult.getStacks()

		String stackName = this.context.get("aws_stack_name")

		log.info(stacks[0].getStackName() + " - Current Status: " + stacks[0].getStackStatus() + " - Target Status: " + StackStatus.CREATE_COMPLETE.toString())

		if ((stacks.size() == 1) && (stacks[0]).getStackName() == stackName) {
			return true
		} else {
			return true // TODO: Do some defensive programming here
		}

	} // END doesStackExist Method


	private boolean stackCompleted() {

		DescribeStacksResult describeStacksResult = null

		try {
			describeStacksResult = this.describeStack()
		} catch (AmazonServiceException ase) {
			return false
		}

		List<Stack> stacks = describeStacksResult.getStacks()

		String stackName = this.context.get("aws_stack_name")

		log.info("Creating Stack... " + stacks[0].getStackName() + " - Current Status: " + stacks[0].getStackStatus() + " - Target Status: " + StackStatus.CREATE_COMPLETE.toString())


		if ((stacks.size() == 1) && (stacks[0].getStackName() == stackName) && (stacks[0].getStackStatus() == StackStatus.CREATE_COMPLETE.toString())) {

			String bucketName = this.context.get("deploymentBucket")
			AWSCredentials awsCredentials = this.context.getAWSCredentials()
			AmazonS3Client s3Client = new AmazonS3Client(awsCredentials)
			s3Client.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

			S3Object object = s3Client.getObject(
					new GetObjectRequest(bucketName, "kpFacterProps.json"))
			InputStream objectData = object.getObjectContent()

			BufferedReader reader = new BufferedReader(new InputStreamReader(objectData))
			JsonSlurper jsonSlurper = new JsonSlurper()
			Map jsonResult = (Map) jsonSlurper.parse(reader)

			def stack = stacks[0].getOutputs()
			def output = stack.each { out ->
				jsonResult.put(out.outputKey, out.outputValue)
				this.context.add(out.outputKey, out.outputValue)
			}

			log.debug("Context: " + this.context)

			File propFile = new File(System.getProperty("user.dir") + File.separator + "kpFacterProps.json")
			String json = JsonOutput.prettyPrint(JsonOutput.toJson(jsonResult))

			propFile.write json
			objectData.close()
			s3Client.putObject(new PutObjectRequest(
					bucketName, "kpFacterProps.json", propFile))
			propFile.delete()
			return true

		} else {

			return false
		}
	} // END stackCompleted Method
	/*

	 */
	private void getStackOutput() {

		DescribeStacksResult describeStacksResult = null

		try {
			describeStacksResult = this.describeStack()
		} catch (AmazonServiceException ase) {
			throw new MinionException("There was an error describing the cloudformation stack", ase)
		}

		List<com.amazonaws.services.cloudformation.model.Stack> stacks = describeStacksResult.getStacks()

		String stackName = this.context.get("aws_stack_name")
		if ((stacks.size() == 1) && (stacks[0].getStackName() == stackName) && (stacks[0].getStackStatus() == StackStatus.CREATE_COMPLETE.toString())) {
			def stack = stacks[0].getOutputs()
			def output = stack.each {
				out ->
					this.context.add(out.outputKey, out.outputValue)
					System.out.println("***** cloudformation: " + out.outputKey + " : " + out.outputValue)

			}
		}
	}


} // END CloudFormationMinion class
