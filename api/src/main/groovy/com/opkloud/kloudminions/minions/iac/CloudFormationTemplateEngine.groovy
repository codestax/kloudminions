package com.opkloud.kloudminions.minions.iac

import groovy.text.GStringTemplateEngine;
import groovy.text.Template

class CloudFormationTemplateEngine {


	/**
	 * 	
	 * @param cloudFormationFile
	 * @param model
	 * @return
	 */
	public String process(File cloudFormationFile, Map systemParams, Map model) {
	
		cloudFormationFile.setReadOnly()
	
		GStringTemplateEngine engine = new GStringTemplateEngine()
		Template template = engine.createTemplate(cloudFormationFile)
		Writable cfContents = template.make(model)
		String cfString = cfContents.toString()
		return cfString
	
	}


	public String process(File cloudFormationFile, Map model) {
		return this.process(cloudFormationFile, null, model)
	}
	
	

	
}




//public class TemplateWrapper implements Template {
//
//	private Template internalTemplate
//	
//	public TemplateWrapper(Template internalTemplate) {
//		this.internalTemplate = internalTemplate
//	}
//	
//	@Override
//	public Writable make() {
//		return this.internalTemplate.make()
//	}
//
//	@Override
//	public Writable make(Map binding) {
//		return this.internalTemplate.make(binding);
//	}
//	
//
//	
//		
//	
//
//}
