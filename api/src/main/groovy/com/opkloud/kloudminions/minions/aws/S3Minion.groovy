package com.opkloud.kloudminions.minions.aws

import com.amazonaws.AmazonClientException
import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.BucketVersioningConfiguration
import com.amazonaws.services.s3.model.DeleteObjectRequest
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.services.s3.model.SetBucketVersioningConfigurationRequest
import com.opkloud.kloudminions.minions.AbstractMinion
import com.opkloud.kloudminions.minions.KloudMinion
import com.opkloud.kloudminions.exceptions.MinionException
import groovy.transform.InheritConstructors
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.nio.file.Path
import java.nio.file.Paths

@InheritConstructors
@KloudMinion(name="s3_minion")
class S3Minion extends AbstractMinion {

	private static final def UPLOADING_MSG = "Uploading %s to S3!"
	private static final def DEPLOYMENTS_NOT_FOUND_MSG = "The following file(s) could not be uploaded to S3 because they were not found: %s"

	private static final Logger log = LoggerFactory.getLogger(S3Minion.class)

	/**
	 * Uploads files to S3
	 *
	 */
	void uploadFiles(){


        AWSCredentials awsCredentials = this.context.getAWSCredentials()
//        AmazonS3Client s3client = new AmazonS3Client(awsCredentials)

//		AmazonS3ClientBuilder.standard().withCredential().withRegion("eu-west-1").build();
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
				.build();


//		s3Client.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

		List<String> files = this.context.get("files")
		String s3Dir = this.context.get("s3_dir")

		try {

			if (!s3Client.doesBucketExist(getS3BucketName())) {
				s3Client.createBucket(getS3BucketName())

			}
			s3Client.setBucketVersioningConfiguration(buildS3BucketVersioningRequest())

			files.forEach{
				filePath ->
					Path path = Paths.get(filePath.toString())

					log.info("Uploading " + path.getFileName().toString() + " to S3!") // TODO this was originally a printf statement

					if(s3Dir != null){
						s3Client.putObject(new PutObjectRequest(
								getS3BucketName(),
								s3Dir + "/" + path.getFileName().toString(),
								path.toFile()))
					}
					else{
						s3Client.putObject(new PutObjectRequest(
								getS3BucketName(),
								path.getFileName().toString(),
								path.toFile()))
					}

			}

		} catch (AmazonServiceException ase) {
			throw new MinionException(ase.message, ase)
		} catch (AmazonClientException ace) {
			throw new MinionException(ace.message, ace)
		}catch (IllegalArgumentException iae) {
			throw new MinionException(iae.message, iae)
		}
	}

	/**
	 * Deletes the specified file from the configured S3 bucket.
	 * @param fileName file to delete
	 *
	 */
	void deleteFile(def fileName){
        AWSCredentials awsCredentials = this.context.getAWSCredentials()
        AmazonS3Client s3client = new AmazonS3Client(awsCredentials)
        s3client.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

		s3client.deleteObject(new DeleteObjectRequest(getS3BucketName(), fileName))
	}


	private def buildS3BucketVersioningRequest(){
		return new SetBucketVersioningConfigurationRequest(getS3BucketName(),
		new BucketVersioningConfiguration(BucketVersioningConfiguration.ENABLED))
	}

	private String getS3BucketName() {
		return this.context.get("bucket")
	}


	@Override
	public void execute() {
		// TODO Auto-generated method stub
	}


} // END S3Minion Class
