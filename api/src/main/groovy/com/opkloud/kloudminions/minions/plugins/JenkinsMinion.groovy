package com.opkloud.kloudminions.minions.plugins

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.DescribeImagesRequest
import com.amazonaws.services.ec2.model.DescribeImagesResult
import com.amazonaws.services.ec2.model.Filter
import com.amazonaws.services.ec2.model.Image
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.S3Object
import com.opkloud.kloudminions.context.Context
import com.opkloud.kloudminions.minions.AbstractMinion
import com.opkloud.kloudminions.minions.KloudMinion
import com.opkloud.kloudminions.exceptions.MinionException
import groovy.json.JsonOutput
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.nio.file.Files
import java.nio.file.Paths

@KloudMinion(name="jenkins_minion")
class JenkinsMinion extends AbstractMinion {

    private AWSCredentials awsCredentials = this.context.getAWSCredentials()

	private static final Logger log = LoggerFactory.getLogger(JenkinsMinion.class)


    JenkinsMinion(Context context) {
        super(context)
	}

	
	@Override
	void execute() {
		// TODO Auto-generated method stub
	}

    void createEc2PluginHieraYaml(){

        String amiId = this.context.get("amiId")
        String keyName = this.context.get("keyName")
        String securityGroupName = this.context.get("slaveSecurityGroupName")
        String subnetId = this.context.get("subnetId")

        Map<String, Object> data = new HashMap<String, Object>()
        data.put("amiId", amiId)
        data.put("privateKey",  getPrivateKey())
        data.put("securityGroupName", securityGroupName)
        data.put("subnetId", subnetId)

        Map<String, Object> data2 = new HashMap<String, Object>()

        data2.put("ec2PluginValues", data)

        String content = JsonOutput.toJson(data2)
        Files.write(Paths.get("./infrastructure/puppet/manifests/common.json"), content.getBytes());




    }

    private void getLatestAmazonLinux(){

       AmazonEC2 ec2Client = new AmazonEC2Client(awsCredentials)

        DescribeImagesResult imagesResult = ec2Client.describeImages(new DescribeImagesRequest().withFilters(new Filter().withName("architecture").withValues("x86_64"),
        new Filter().withName("owner-alias").withValues("amazon"),
        new Filter().withName("description").withValues("Amazon Linux AMIw"),
        new Filter().withName("root-device-type").withValues("ebs")))

        List<Image> images = imagesResult.getImages()

        images.each { image ->
            System.out.println(image.name)
            System.out.println(image.imageId)
            System.out.println(image.architecture)
            System.out.println(image.description)
            System.out.println(image.creationDate)
            System.out.println(image.rootDeviceType)
            System.out.println(image.platform)
            System.out.println("")
        }


    }

    private String getPrivateKey(){
        AmazonS3 amazonS3 = new AmazonS3Client(awsCredentials)
        amazonS3.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

        if(!amazonS3.doesObjectExist(this.context.get("bucketName"),this.context.get("keyName") + ".pem")){
            throw new MinionException("Private key file does not exist in S3!")
        }

        log.info("Retrieving private key from s3...")

        S3Object s3Object = amazonS3.getObject(this.context.get("bucketName"),this.context.get("keyName") + ".pem")

        BufferedReader reader = new BufferedReader(new InputStreamReader(s3Object.getObjectContent()))
        String content = ""
        String line ="";
        while((line = reader.readLine()) != null) {

            content += line+ "\\n"

        }

        log.info("Retrieved private key from s3...")

        return StringUtils.chomp(content.trim())

    }

}
