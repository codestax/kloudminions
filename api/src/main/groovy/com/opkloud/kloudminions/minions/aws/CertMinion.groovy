package com.opkloud.kloudminions.minions.aws

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.certificatemanager.AWSCertificateManager
import com.amazonaws.services.certificatemanager.AWSCertificateManagerClient
import com.amazonaws.services.certificatemanager.model.ListCertificatesRequest
import com.amazonaws.services.certificatemanager.model.ListCertificatesResult
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancing
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClient
import com.amazonaws.services.elasticloadbalancing.model.CreateLoadBalancerListenersRequest
import com.amazonaws.services.elasticloadbalancing.model.Listener
import com.amazonaws.services.elasticloadbalancing.model.SetLoadBalancerListenerSSLCertificateRequest
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient
import com.amazonaws.services.identitymanagement.model.DeleteServerCertificateRequest
import com.amazonaws.services.identitymanagement.model.GetServerCertificateRequest
import com.amazonaws.services.identitymanagement.model.GetServerCertificateResult
import com.amazonaws.services.identitymanagement.model.UploadServerCertificateRequest
import com.opkloud.kloudminions.context.Context
import com.opkloud.kloudminions.minions.AbstractMinion
import com.opkloud.kloudminions.minions.KloudMinion
import com.opkloud.kloudminions.exceptions.MinionException
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@KloudMinion(name="cert_minion")
class CertMinion extends AbstractMinion {

    private AWSCredentials awsCredentials = this.context.getAWSCredentials()

	private static final Logger log = LoggerFactory.getLogger(CertMinion.class)


    CertMinion(Context context) {
        super(context)
	}

	
	@Override
	void execute() {
		// TODO Auto-generated method stub
	}

    void applyCertManagerCert(){

        String loadBalancerName = this.context.get("loadBalancerName")
        if(StringUtils.isBlank(loadBalancerName)) {
            throw new MinionException("Loadbalancer name cannot be empty or null")
        }
        String certArn = getCertManagerCert()

        AmazonElasticLoadBalancing elb = new AmazonElasticLoadBalancingClient(awsCredentials)

        createELBListenerRequest(certArn, loadBalancerName, elb)

        elb.setLoadBalancerListenerSSLCertificate(createElbCertRequest(loadBalancerName, certArn))

    }

    void applyIamCert(){

        String loadBalancerName = this.context.get("loadBalancerName")
        if(StringUtils.isBlank(loadBalancerName)) {
            throw new MinionException("Loadbalancer name cannot be empty or null")
        }
        String certArn = getIamCert()

        AmazonElasticLoadBalancing elb = new AmazonElasticLoadBalancingClient(awsCredentials)

        createELBListenerRequest(certArn, loadBalancerName, elb)

        elb.setLoadBalancerListenerSSLCertificate(createElbCertRequest(loadBalancerName, certArn))

    }


    private void createELBListenerRequest(String certArn, String loadBalancerName, AmazonElasticLoadBalancingClient elb) {

        CreateLoadBalancerListenersRequest lblReq = new CreateLoadBalancerListenersRequest()

        lblReq.withListeners(Arrays.asList(createELBSslListener(certArn))).withLoadBalancerName(loadBalancerName)

        elb.createLoadBalancerListeners(lblReq)
    }


    private Listener createELBSslListener(String certArn) {
        Listener listener = new Listener()

        listener.withProtocol("HTTPS")
				.withLoadBalancerPort(443).
				withSSLCertificateId(certArn).
				withInstanceProtocol("HTTP").
				withInstancePort(80)

		return listener
    }


    private SetLoadBalancerListenerSSLCertificateRequest createElbCertRequest(String loadBalancerName, String certArn) {
        SetLoadBalancerListenerSSLCertificateRequest elbSSLCertReq = new SetLoadBalancerListenerSSLCertificateRequest()

        elbSSLCertReq.withLoadBalancerName(loadBalancerName)
                	.withLoadBalancerPort(443)
                	.withSSLCertificateId(certArn)

        return elbSSLCertReq
    }


    String getCertManagerCert() {
        // Create an AWSCertificateManager client and set the region.
        AWSCertificateManager acm = new AWSCertificateManagerClient(awsCredentials);
        acm.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

        // Call the listCertificates function.
        ListCertificatesResult res = acm.listCertificates(new ListCertificatesRequest());


        String certArn = null

        res.getCertificateSummaryList().forEach{t ->
			if(t.getDomainName().equalsIgnoreCase(this.context.get("certDomainName"))) {
            	certArn = t.getCertificateArn()
        	}
		}

        log.debug(certArn);

        if(certArn == null) {
            throw new MinionException("PLEASE CONFIGURE CERT BEFORE RUNNING!")
        }

        return certArn

	}

    String getIamCert() {
        // Create an AWSCertificateManager client and set the region.
//        AmazonIdentityManagement aim = new AmazonIdentityManagementClient(awsCredentials)
//        acm.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))

        String certName = this.context.get("certName")


        GetServerCertificateResult cert = getServerCert(certName)

        String certArn = cert.serverCertificate.serverCertificateMetadata.arn

        System.out.println(certArn);

        if(certArn == null) {
            throw new MinionException("specified cert name does not exist!")
        }

        return certArn

    }

    private GetServerCertificateResult getServerCert(String certName) {
        AmazonIdentityManagement aim = new AmazonIdentityManagementClient(awsCredentials)
        aim.setRegion(RegionUtils.getRegion(this.context.get("aws_region")))
        aim.getServerCertificate(new GetServerCertificateRequest().withServerCertificateName(certName))
    }


    void uploadServerCert(){

        AmazonIdentityManagement aim = new AmazonIdentityManagementClient(awsCredentials)

        String certBody = new File(this.context.get("certPath")).text
        String privateKeyBody = new File(this.context.get("privateKeyPath")).text
        String certChainBody =  new File(this.context.get("certChainPath")).text
        String certName = this.context.get("certName")
        String pathName = this.context.get("pathName")

        //check if cert exists

        aim.uploadServerCertificate(getServerCertRequest(certBody,privateKeyBody,certChainBody,certName,pathName))
    }

    void deleteServerCert(){

        AmazonIdentityManagement aim = new AmazonIdentityManagementClient(awsCredentials)

        String certName = this.context.get("certName")

        //check if cert exists

        aim.deleteServerCertificate(new DeleteServerCertificateRequest().withServerCertificateName(certName))
    }

    private UploadServerCertificateRequest getServerCertRequest(String certBody,String privateKeyBody,String certChainBody,String certName,String path){

        return new UploadServerCertificateRequest()
                    .withCertificateBody(certBody)
                    .withPrivateKey(privateKeyBody)
                    .withServerCertificateName(certName)
                    .withCertificateChain(certChainBody)
                    .withPath(path)

    }

}
