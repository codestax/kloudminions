package com.opkloud.kloudminions.exceptions

import groovy.transform.InheritConstructors


@InheritConstructors
class ActionException extends Exception {

}
