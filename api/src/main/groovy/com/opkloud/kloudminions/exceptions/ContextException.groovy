package com.opkloud.kloudminions.exceptions

import groovy.transform.InheritConstructors;

@InheritConstructors
class ContextException extends Exception {

}
