package com.opkloud.kloudminions.utils

import groovy.json.JsonSlurper

import org.apache.commons.lang3.StringUtils

import com.opkloud.kloudminions.exceptions.MinionUtilityException
import org.slf4j.Logger
import org.slf4j.LoggerFactory


class MinionUtils {

	public static final CURRENT_DIRECTORY = System.getProperty("user.dir")

	public static final DEFAULT_INFRASTRUCTURE_DIRECTORY = "src" + File.separator + "main" + File.separator + "infrastructure"
	
	public static final TEST_AWS_CREDENTIALS_POINTER_FILE = "./aws_credentials_pointer_file.json"

	private static final Logger log = LoggerFactory.getLogger(MinionUtils.class)
	
	
	static Map readJsonFromFile(String filePath) {

		File file = new File(filePath)

		return readJsonFromFile(file)

	} // END readJsonFromFile Method



	public static Map readJsonFromFile(File file) {

		file.setReadOnly()
		String fileContents = file.text
		if(StringUtils.isBlank(fileContents)){
			throw new MinionUtilityException(file.canonicalPath + " can not be empty!")
		}

		JsonSlurper slurper = new JsonSlurper()
		return slurper.parseText(fileContents)

	} // END readJsonFromFile Method


	static File getCurrentDirectory() {

		return new File(".")
	}



//
//	public static File findConfigFile() {
//
//
//
//	}

}
