package com.opkloud.kloudminions.registry

import com.opkloud.kloudminions.minions.Minion
import org.slf4j.Logger
import org.slf4j.LoggerFactory;

/**
 * Shortcut methods for the MinionRegistry Class
 *
 */
class MinReg {


	private static final Logger log = LoggerFactory.getLogger(MinReg.class)

	static String get(String minionType) {
		String classNameString = MinionRegistry.getMinionClassName(minionType)
		return classNameString
	}


	static void add(String minionType, Minion minionObject) {
		
		MinionRegistry.registerMinionType(minionType, minionObject)
	}


	static void add(String minionType, Class minionClass) {

		MinionRegistry.registerMinionType(minionType, minionClass)
	}


	static void add(String minionType, String minionClassName) {

		MinionRegistry.registerMinionType(minionType, minionClassName)
	}
	
}
