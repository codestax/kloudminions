package com.opkloud.kloudminions.registry

import io.github.classgraph.AnnotationInfo
import io.github.classgraph.AnnotationParameterValue
import io.github.classgraph.ClassGraph
import io.github.classgraph.ClassInfo
import io.github.classgraph.ScanResult
import org.apache.commons.lang3.StringUtils

import com.opkloud.kloudminions.minions.Minion

import org.slf4j.Logger
import org.slf4j.LoggerFactory



class MinionRegistry {

    private static Map<String, String> minionRegister = null
    private static Object lock = new Object()


    private static final Logger log = LoggerFactory.getLogger(MinionRegistry.class)

    /*
        TODO: Refactor to support various environmental tags.  Example: only include this minion when the environment is test, or dev or production, etc...
     */

    static Map<String, String> initializeRegister() {

        log.info("Initializing MinionRegistry")
        
        Map defaultRegister = new HashMap()

         ScanResult scanResult =
                new ClassGraph().enableAllInfo()
                        .whitelistPackages("com.opkloud.kloudminions.minions")
                        .scan()

        for (ClassInfo minionClassInfo : scanResult.getClassesWithAnnotation("com.opkloud.kloudminions.minions.KloudMinion")) {
                AnnotationInfo minionAnnotationInfo = minionClassInfo.getAnnotationInfo("com.opkloud.kloudminions.minions.KloudMinion");
                List<AnnotationParameterValue> minionParamVals = minionAnnotationInfo.getParameterValues();
                // @KloudMinion has one required parameter
                String name = (String) minionParamVals.get(0).getValue();
                defaultRegister.put(name, minionClassInfo.getName())
                log.info("Registered Minion - Class Name: " + minionClassInfo.getSimpleName() + "   ---  Minion Value: " + name)

            }

        scanResult.close()

        log.info("END Minion Register Size: " +defaultRegister.size())

        MinionRegistry.minionRegister = defaultRegister

    }

    static String getMinionClassName(String minionType) {


        if (MinionRegistry.minionRegister == null) {
            synchronized (MinionRegistry.lock) {
                MinionRegistry.initializeRegister()
            }
        }

        return MinionRegistry.minionRegister.get(minionType)
    }


    static void registerMinionType(String minionType, Minion minionObject) {

        if (minionObject == null) {
            throw new IllegalArgumentException("ERROR: In MinionRegistry.registerMinionType minionObject argument is null")
        }

        Class minionClass = minionObject.class
        MinionRegistry.registerMinionType(minionType, minionClass)
    }


    static void registerMinionType(String minionType, Class minionClass) {

        if (minionClass == null) {
            throw new IllegalArgumentException("ERROR: In MinionRegistry.registerMinionType minionClass argument is null")
        }

        String minionClassName = minionClass.name
        MinionRegistry.registerMinionType(minionType, minionClassName)
    }


    static void registerMinionType(String minionType, String minionClassName) {

        if (StringUtils.isBlank(minionType)) {
            throw new IllegalArgumentException("ERROR: In MinionRegistry.registerMinionType minionType argument is null, empty string or blank")
        }

        if (StringUtils.isBlank(minionClassName)) {
            throw new IllegalArgumentException("ERROR: In MinionRegistry.registerMinionType minionClassName argument is null, empty string or blank")
        }

        if (MinionRegistry.minionRegister == null) {
            synchronized (MinionRegistry.lock) {
                MinionRegistry.initializeRegister()
            }
        }

        MinionRegistry.minionRegister.put(minionType, minionClassName)
    }

} // END MinionRegistry Class
