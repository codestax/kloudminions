package com.opkloud.kloudminions.executable

trait Executable {

	abstract void execute()
	
} // END Executable trait