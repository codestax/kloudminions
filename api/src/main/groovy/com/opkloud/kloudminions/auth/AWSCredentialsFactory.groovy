package com.opkloud.kloudminions.auth

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.services.cloudfront.model.InvalidArgumentException
import com.opkloud.kloudminions.utils.MinionUtils
import groovy.json.JsonSlurper
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class AWSCredentialsFactory {


    public static final String AWS = "aws"

    public static final String DEV = "dev"
    public static final String TEST = "test"
    public static final String PROD = "prod"

	private static final Logger log = LoggerFactory.getLogger(AWSCredentialsFactory.class)


    public static AWSCredentials getInstance(String platform, String environment) {

		/*
			TODO Accept a list of strings that generate a "_" delimited file name
		 */


        if (StringUtils.isBlank(platform)) {
            throw new InvalidArgumentException("AWSCredentials:getInstance(String platform, String environment) - argument 'platform' is blank")
        }
        if (StringUtils.isBlank(environment)) {
            throw new InvalidArgumentException("AWSCredentials:getInstance(String platform, String environment) - argument 'environment' is blank")
        }

        // TODO: Do some defensive programming here to protect against random directory paths being injected in these variable (perhaps strip out everything except alphabets
        platform = platform.trim().toLowerCase()
        environment = environment.trim().toLowerCase()

        // TODO: Move these 'magic' strings to the config file system once it is created.



        String awsCredentialsPointerFile = MinionUtils.TEST_AWS_CREDENTIALS_POINTER_FILE
        Map awsCredentialsPointerMap = MinionUtils.readJsonFromFile(awsCredentialsPointerFile)

        String awsCredentialsFilePath = awsCredentialsPointerMap.get("aws_credentials_file_path")

        String filename = awsCredentialsFilePath + "/" + platform + "_" + environment + "_" + "credentials.json"

        AWSCredentials awsCredentials = AWSCredentialsFactory.getFromFile(filename)

        return awsCredentials
    }


    /**
     * Platform defaults to AWS
     *
     * @param environment
     * @return
     */
    public static AWSCredentials getInstance(String environment) {

        if (StringUtils.isBlank(environment)) {
            throw new InvalidArgumentException("AWSCredentials:getInstance(String environment) - argument 'environment' is blank")
        }

        AWSCredentials awsCredentials = AWSCredentialsFactory.getInstance(AWSCredentialsFactory.AWS, environment)
        return awsCredentials

    }


    public static AWSCredentials getFromFile(String credentialsFilePath) {

        if (StringUtils.isBlank(credentialsFilePath)) {
            throw new InvalidArgumentException("AWSCredentials:getInstance(String platform, String environment) - argument 'credentialsFilePath' is blank")
        }

        File credentialsFile = new File(credentialsFilePath)
        String credentialsFileContent = credentialsFile.getText()

        JsonSlurper jsonSlurper = new JsonSlurper();
        Map credentialsMap = jsonSlurper.parseText(credentialsFileContent)


        String awsProviderType = credentialsMap["aws_provider_type"]
        AWSCredentials awsCredentials = null

        if (StringUtils.isBlank(awsProviderType) || (awsProviderType == "BASIC")) {
            String awsAccessKey = credentialsMap["aws_access_key"]
            String awsSecretKey = credentialsMap["aws_secret_key"]

            BasicCredentialsProvider basicCredentialsProvider = new BasicCredentialsProvider(awsAccessKey, awsSecretKey)
            awsCredentials = basicCredentialsProvider.getCredentials()

        } else {

            // Assume it's a fully qualified class that implements the AWSCredentialsProvider interface

            // TODO: Maybe create a better interface to allow for passing in a config object

            log.debug("Creating Provider! " + awsProviderType)
            Class clazz = awsProviderType as Class
            AWSCredentialsProvider awsCredentialsProvider = (AWSCredentialsProvider) clazz.newInstance(credentialsMap)
            awsCredentials = awsCredentialsProvider.getCredentials()

        }

        return awsCredentials
    }


} // END AWSCredentialsFactory Class
