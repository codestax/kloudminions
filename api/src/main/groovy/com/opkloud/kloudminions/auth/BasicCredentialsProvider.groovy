package com.opkloud.kloudminions.auth

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by rashadmoore on 3/31/16.
 */
class BasicCredentialsProvider implements AWSCredentialsProvider {

    private awsAccessKey
    private awsSecretKey

	private static final Logger log = LoggerFactory.getLogger(BasicCredentialsProvider.class)

    public BasicCredentialsProvider (String awsAccessKey, String awsSecretKey) {
        this.awsAccessKey = awsAccessKey
        this.awsSecretKey = awsSecretKey
    }

    @Override
    public AWSCredentials getCredentials() {
        AWSCredentials awsCredentials = new BasicAWSCredentials(this.awsAccessKey, this.awsSecretKey)
        return awsCredentials
    }

    @Override
    public void refresh() {
        // DO NOTHING
    }


}
