package com.opkloud.kloudminions.process

import com.opkloud.kloudminions.context.LocalContext
import org.apache.commons.lang3.StringUtils

import com.opkloud.kloudminions.exceptions.ActionException
import com.opkloud.kloudminions.registry.MinReg
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ActionNode extends ProcessNode {

	String minionClassName
	String minionActionName
	def  minionObject

	private static final Logger log = LoggerFactory.getLogger(ActionNode.class)

		
	ActionNode(Map<String, Object> processNodeSpecObj, LocalContext localContext) {
	
		super(processNodeSpecObj, localContext)

		if (StringUtils.isBlank(processNodeSpecObj.action)) {
			throw new IllegalArgumentException("ERROR: processNodeSpecObj.action is blank")
		}

		this.resolveActionReference(processNodeSpecObj.action)
		
	}
	
	
	void execute() {

		/*
		 * TODO: Look up the default method to execute if minionActionName is Blank.  If no
		 * default action is specified in the MinionClass then throw some sort of exception
		 */
		
		this.minionObject."$minionActionName"()
		
	}
	
	
	static ActionNode createInstance (Map processNodeSpecObj, LocalContext localContext) {
		
				
		if (StringUtils.isBlank(processNodeSpecObj.type)) {
			throw new IllegalArgumentException("ERROR: In ActionNode.createInstance processNodeSpecObj.type is either null, empty string or all spaces")
		}
		
		if (processNodeSpecObj.type != "action") {
			throw new IllegalArgumentException("ERROR: In ActionNode.createInstance processNodeSpecObj.type is not equal to 'action'")
		}
		
		if (StringUtils.isBlank(processNodeSpecObj.action)) {
			throw new IllegalArgumentException("ERROR: In ActionNode.createInstance processNodeSpecObj.action is either null, empty string or all spaces")
		}
		
		if (localContext == null) {
			throw new IllegalArgumentException("ERROR: In ActionNode.createInstance localContext is null")
		}
		
		ActionNode actionNode = new ActionNode(processNodeSpecObj, localContext)
		return actionNode
	}
	
	
	def resolveActionReference (String actionReference) {
		
		if (StringUtils.isBlank(actionReference)) {
			throw new ActionException("ERROR: In SimpleAction.minion minionReference is null, empty string or all whitespace")
		}
		
		ArrayList<String> minionRefList = actionReference.split(":")
		this.minionClassName = minionRefList[0]
		this.minionActionName = minionRefList[1]
		
		if (StringUtils.isNotBlank(MinReg.get(this.minionClassName))) {
			this.minionClassName = MinReg.get(this.minionClassName)
		}

		
		this.minionObject = Class.forName(this.minionClassName)?.newInstance(this.localContext)
		
	}

	

} // END ActionNode Class
