package com.opkloud.kloudminions.process

import com.amazonaws.auth.AWSCredentials
import com.opkloud.kloudminions.context.GlobalContext
import com.opkloud.kloudminions.executable.Executable
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.MDC

class Process implements Executable {

    Map<String, Object> processSpecMap
    GlobalContext globalContext
    ProcessNode rootProcessNode


    private static final Logger log = LoggerFactory.getLogger(Process.class)


    private Process() {
        // PRIVATE -- Empty do nothing constructor to support usage in the "withThis" build method
    }


    static ProcessBuilder getBuilder() {

        ProcessBuilder processBuilder = new ProcessBuilder()
        return processBuilder
    }


    static Process createInstance(Map processSpecMap, AWSCredentials awsCredentials, Map globalContextMap) {


        Process process = Process.getBuilder().setGlobalContextMap(globalContextMap)
                .setAWSCredentials(awsCredentials)
                .setProcessSpec(processSpecMap)
                .build()

        return process
    }


    @Override
    public void execute() {

        MDC.put("process_id", UUID.randomUUID().toString());

        try {

            log.trace("Starting Execution of process")

            this.rootProcessNode.execute()

            log.trace("Completed Execution of process")

        } catch (Exception exception) {

            log.error("Generic Process Exception", exception)

        } finally {

            log.trace("Clearing MDC")
            MDC.clear()
        }

    } // END execute Method

    /*
        TODO Confirm this...
        Package-Protected methods
     */

    void setProcessSpecMap(Map<String, Object> processSpecMap) {
        this.processSpecMap = processSpecMap
    }


    void setGlobalContext(GlobalContext globalContext) {
        this.globalContext = globalContext
    }


    GlobalContext getGlobalContext() {
        return globalContext
    }


    void setRootProcessNode(ProcessNode rootProcessNode) {
        this.rootProcessNode = rootProcessNode
    }


    ProcessNode getRootProcessNode() {
        return this.rootProcessNode
    }

} // END Process Class
