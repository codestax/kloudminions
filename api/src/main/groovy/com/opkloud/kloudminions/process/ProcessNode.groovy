package com.opkloud.kloudminions.process

import com.opkloud.kloudminions.context.GlobalContext
import com.opkloud.kloudminions.context.LocalContext
import com.opkloud.kloudminions.executable.Executable
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

abstract class ProcessNode implements Executable {


	protected Map<String, Object> processNodeSpecObj
	protected LocalContext localContext
	private String typeDefinition

	private static final Logger log = LoggerFactory.getLogger(ProcessNode.class)


	ProcessNode() {
		// TODO: Prevent creation of a ProcessNode with an empty constructor
	}


	ProcessNode(Map<String, Object> processNodeSpecObj, LocalContext localContext) {

		this.processNodeSpecObj = processNodeSpecObj
		this.localContext = localContext
		this.typeDefinition = processNodeSpecObj["type"]
	}


	static ProcessNode createInstance (Map<String, Object> processNodeSpecObj, GlobalContext globalContext) {

		if (processNodeSpecObj == null) {
			throw new IllegalArgumentException("ERROR: In ProcessNode.createInstance processNodeSpecObj is either null")
		}

		if (StringUtils.isBlank(processNodeSpecObj.type)) {
			throw new IllegalArgumentException("ERROR: In ProcessNode.createInstance processNodeSpecObj.type is either null, empty string or all spaces")
		}
		
		if (globalContext == null) {
			throw new IllegalArgumentException("ERROR: In ProcessNode.createInstance globalContext is null")
		}
		
		ProcessNode processNode


		String contextId = processNodeSpecObj.get("id")

		LocalContext localContext = globalContext.createLocalContext(contextId, processNodeSpecObj)
		
		if (processNodeSpecObj.type == "sequence") {
			
			processNode = SequenceNode.createInstance(processNodeSpecObj, localContext)
			
		} else if (processNodeSpecObj.type == "action") {
		
			processNode = ActionNode.createInstance(processNodeSpecObj, localContext)
		
		}  else if (processNodeSpecObj.type == "fork") {

			// TODO Create Fork Node
			throw new Exception("Fork is not a valid node type")
//			processNode = ActionNode.createInstance(processNodeSpecObj, context)

		}

		return processNode
	}


	LocalContext getLocalContext() {
		return this.localContext
	}


	String getTypeDefinition() {
		return this.typeDefinition
	}


} // END ProcessNode Class
