package com.opkloud.kloudminions.process

import com.amazonaws.auth.AWSCredentials
import com.opkloud.kloudminions.context.GlobalContext
import com.opkloud.kloudminions.registry.MinionRegistry
import groovy.json.JsonSlurper
import org.slf4j.Logger
import org.slf4j.LoggerFactory


class ProcessBuilder {


    private Map<String, Object> processSpecMap = null
    private Map<String, Object> globalContextMap = new HashMap<String, Object>()
    private AWSCredentials awsCredentials = null

	private static final Logger log = LoggerFactory.getLogger(ProcessBuilder.class)

    ProcessBuilder() {
		// Do Nothing Constructor
	}


    ProcessBuilder setProcessSpec(Map<String, Object> processSpecMap) {
        this.processSpecMap = processSpecMap
		return this
    }


    ProcessBuilder setProcessSpec(String processSpecString) {

        JsonSlurper jsonSlurper = new JsonSlurper();
        Map<String, Object> processSpecMap = jsonSlurper.parseText(processSpecString)
		ProcessBuilder pb = this.setProcessSpec(processSpecMap)
		return pb
    }


    ProcessBuilder setGlobalContextMap(Map<String, Object> globalContextMap) {
        this.globalContextMap = globalContextMap
		return this
    }


    ProcessBuilder addGlobalContextMap(Map<String, Object> globalContextMap) {
        this.globalContextMap.putAll(globalContextMap)
        return this
    }


    ProcessBuilder setGlobalContextMap(String globalContextMapString) {

        JsonSlurper jsonSlurper = new JsonSlurper();
        Map<String, Object> globalContextMap = jsonSlurper.parseText(globalContextMapString)
		ProcessBuilder pb = this.setGlobalContextMap(globalContextMap)
		return pb
    }


    ProcessBuilder addGlobalContextMap(String globalContextMapString) {

        JsonSlurper jsonSlurper = new JsonSlurper();
        Map<String, Object> globalContextMap = jsonSlurper.parseText(globalContextMapString)
        ProcessBuilder pb = this.addGlobalContextMap(globalContextMap)
        return pb
    }

    ProcessBuilder setAWSCredentials(AWSCredentials awsCredentials) {
        this.awsCredentials = awsCredentials
		return this
    }


    Process build() {

		MinionRegistry.initializeRegister()

		Process process = new Process()

		GlobalContext globalContext = GlobalContext.getInstance(this.awsCredentials, this.globalContextMap)
		globalContext.setProcess(process)

		process.setGlobalContext(globalContext)


		process.setProcessSpecMap(this.processSpecMap)


		ProcessNode rootProcessNode = ProcessNode.createInstance(this.processSpecMap, globalContext)
		process.setRootProcessNode(rootProcessNode)


        return process
    }


} // END ProcessBuilder Class
