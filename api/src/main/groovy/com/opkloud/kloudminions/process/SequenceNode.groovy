package com.opkloud.kloudminions.process

import com.opkloud.kloudminions.context.GlobalContext
import com.opkloud.kloudminions.context.LocalContext
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory


class SequenceNode extends ProcessNode {


    List<ProcessNode> nodeList

    private static final Logger log = LoggerFactory.getLogger(SequenceNode.class)


    SequenceNode(Map<String, Object> processNodeSpecObj, LocalContext localContext) {

        super(processNodeSpecObj, localContext)


        this.nodeList = new ArrayList<ProcessNode>()

        processNodeSpecObj.nodes.each { childProcessNodeSpecObj ->

            GlobalContext globalContext = this.localContext.getGlobalContext()
            ProcessNode processNode = ProcessNode.createInstance(childProcessNodeSpecObj, globalContext)
            this.nodeList.add(processNode)
        }

    }


    void execute() {

        this.nodeList.each { processNode ->
            processNode.execute()
        }
    }


    static SequenceNode createInstance(Map processNodeSpecObj, LocalContext localContext) {

        if (StringUtils.isBlank(processNodeSpecObj.type)) {
            throw new IllegalArgumentException("ERROR: In SequenceNode.createInstance processNodeSpecObj.type is either null, empty string or all spaces")
        }

        if (processNodeSpecObj.type != "sequence") {
            throw new IllegalArgumentException("ERROR: In SequenceNode.createInstance processNodeSpecObj.type is not equal to 'sequence'")
        }

        if (processNodeSpecObj.nodes == null) {
            throw new IllegalArgumentException("ERROR: In SequenceNode.createInstance processNodeSpecObj.nodes is null")
        }

        if ((!processNodeSpecObj.nodes?.class?.isArray()) &&
                (!List.isAssignableFrom(processNodeSpecObj.nodes.getClass()))) {
            throw new IllegalArgumentException("ERROR: In SequenceNode.createInstance processNodeSpecObj.nodes is not an array nor is it a List")
        }

        if (localContext == null) {
            throw new IllegalArgumentException("ERROR: In SequenceNode.createInstance localContext is null")
        }

        SequenceNode sequenceNode = new SequenceNode(processNodeSpecObj, localContext)
        return sequenceNode
    }


    List<ProcessNode> getNodeList() {
        return this.nodeList
    }


} // END SequenceNode Class
