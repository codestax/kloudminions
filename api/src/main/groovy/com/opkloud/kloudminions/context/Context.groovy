package com.opkloud.kloudminions.context

import com.amazonaws.auth.AWSCredentials
import com.opkloud.kloudminions.process.Process


interface Context {


	Object get(String key)


	void add(String key, Object value)


	void addAll(Map<String, Object> map)


	/**
	 * A convenience method for quickly adding an entry to a Map<String, Object> that exists within the context already.
	 *
	 * NOTE: If the keyForItemInContextWithAMapValue doesn't exist in the context by default it will be created.  Unless
	 * createIfNotExists is set to false
	 *
	 * @param keyForItemInContextWithAMapValue
	 * @param keyForNewEntry
	 * @param valueForNewEntry
	 * @param createIfNotExists
	 */
	public void addToMap(String keyForItemInContextWithAMapValue, String keyForNewEntry, Object valueForNewEntry, boolean createIfNotExists)


	/**
	 * A convenience method for quickly adding an entry to a Map<String, Object> that exists within the context already.
	 *
	 * NOTE: If the keyForItemInContextWithAMapValue doesn't exist in the context by default a new Map<String, Object> will be created in the context with that key name.
	 *
	 * @param keyForItemInContextWithAMapValue
	 * @param keyForNewEntry
	 * @param valueForNewEntry
	 */
	public void addToMap(String keyForItemInContextWithAMapValue, String keyForNewEntry, Object valueForNewEntry)


	/**
	 * A convenience method for quickly adding an entry to a List<Object> that exists within the context already.
	 *
	 * NOTE: If the keyForItemInContextWithAMapValue doesn't exist in the context by default a new List<Object> will be created in the context with that key name.  Unless
	 * createIfNotExists is set to false
	 *
	 * @param keyForItemInContextWithAListValue
	 * @param keyForNewEntry
	 * @param valueForNewEntry
	 * @param createIfNotExists
	 */
	public void addToList(String keyForItemInContextWithAListValue, Object valueForNewListItem, boolean createIfNotExists)


	/**
	 * A convenience method for quickly adding an entry to a List<Object> that exists within the context already.
	 *
	 * NOTE: If the keyForItemInContextWithAListValue doesn't exist in the context by default a new List will be created in the context with that key name.
	 *
	 * @param keyForItemInContextWithAListValue
	 * @param keyForNewEntry
	 * @param valueForNewEntry
	 */
	void addToList(String keyForItemInContextWithAListValue, Object valueForNewListItem)


	Process getProcess()


	AWSCredentials getAWSCredentials()

}
