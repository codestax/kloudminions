package com.opkloud.kloudminions.context

import com.amazonaws.auth.AWSCredentials
import com.opkloud.kloudminions.process.Process
import groovy.transform.InheritConstructors
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@InheritConstructors
public class GlobalContext extends AbstractContext {


	private HashMap<String, LocalContext> localContexts = new HashMap<String, LocalContext>()
    private AWSCredentials awsCredentials
	private Process process

	public static long LOCAL_CONTEXT_ID_COUNTER = 0

	private static final Logger log = LoggerFactory.getLogger(GlobalContext.class)


	public GlobalContext(AWSCredentials awsCredentials) {

		super()

        if (awsCredentials == null) {
            throw new IllegalArgumentException("awsCredentials argument is null")
        }

        this.awsCredentials = awsCredentials
	}


	public GlobalContext(AWSCredentials awsCredentials, Map<String, Object> globalContextMap) {

		this(awsCredentials);
		this.contextMap.putAll(globalContextMap)
	}


	public GlobalContext(AWSCredentials awsCredentials, Map<String, Object> globalContextMap, Process process) {

		this(awsCredentials, globalContextMap);
		this.process = process
	}



	public void addLocalContext(String contextId, LocalContext localContext) {

		// TODO Defensive Programming

		this.localContexts.put(contextId, localContext)
	}


	public LocalContext getLocalContext(String contextId) {
		return this.localContexts.get(contextId)
	}


	public Map getGlobalContextMap() {
		return this.contextMap
	}


	@Override
	public AWSCredentials getAWSCredentials() {
		return this.awsCredentials
	}


	@Override
	public Object get(String key) {

		// TODO Defensive Programming Needed here

		Object value = this.globalContextMap.get(key)

		if (value != null) {
			return value
		}

		// Key not in global context
		// Check to see if it is in a LocalContext

		String[] keyParts = key.split(":")

		String contextId = null
		String localKey = null

		if ((keyParts != null) && (keyParts.length == 2)) {

			contextId = keyParts[0]
			localKey = keyParts[1]

		} else {

			return null
		}

		if (StringUtils.isBlank(contextId)) {
			throw Exception("context is missing")
		}

		if (StringUtils.isBlank(localKey)) {
			throw Exception("localKey is missing")
		}

		LocalContext localContext = this.localContexts.get(contextId)

		if (localContext == null) {
			throw Exception("ERROR: $contextId does not exist in GlobalContext!!")
		}

		Object localValue = localContext.get(localKey)

		return localValue
	}


	Process getProcess() {
		return process
	}


	void setProcess(Process process) {
		this.process = process
	}


	public int getNumberOfLocalContexts() {
		return this.localContexts.size()
	}


	public static GlobalContext getInstance(AWSCredentials awsCredentials, Map<String, Object> globalContextMap) {
		GlobalContext globalContext = new GlobalContext(awsCredentials, globalContextMap)
		return globalContext
	}


	public LocalContext createLocalContext(String contextId, Map<String, Object> localContextMap) {

		if (StringUtils.isBlank(contextId)) {
			contextId = "context-" + (++GlobalContext.LOCAL_CONTEXT_ID_COUNTER)
		}

		LocalContext localContext = LocalContext.getInstance(contextId, localContextMap, this)
		return localContext
	}


}
