package com.opkloud.kloudminions.context

import com.amazonaws.auth.AWSCredentials
import com.opkloud.kloudminions.process.Process
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class LocalContext extends AbstractContext {


    private String contextId = null
    private GlobalContext globalContext

    private static final Logger log = LoggerFactory.getLogger(LocalContext.class)

    LocalContext(String contextId, GlobalContext globalContext) {

        super()

        // TODO Defensive Programming


        this.contextId = contextId
        this.globalContext = globalContext
        this.globalContext.addLocalContext(this.contextId, this)
    }


    LocalContext(String contextId, Map<String, Object> localContextMap, GlobalContext globalContext) {

        this(contextId, globalContext)

        // TODO Defensive Programming

        this.contextMap.putAll(localContextMap)
    }


    @Override
    Object get(String key) {

        def value = this.contextMap.get(key)

        if (value == null) {
            return this.globalContext.get(key)
        }

        if (value instanceof Map) {
            return this.fixRefsMap(value)
        } else {
            return value
        }

    }


    private Object fixRefsMap(Map<String, Object> value) {

        if (value.ref != null) {

            def referencedKey = value.ref
            def referencedValue = this.get(referencedKey)
            return referencedValue

        } else if ((value instanceof Map) && (value.ref == null)) {

            value = value.collectEntries { localKey, localValue ->

                if (localValue instanceof String) {

                    return [(localKey), localValue]

                } else if (localValue instanceof Map) {

                    return [(localKey), this.fixRefsMap(localValue)]

                } else if (localValue instanceof List) {

                    return [(localKey), this.fixRefsList(localValue)]

                } else {

                    return [(localKey), localValue]
                }

            }

        } else {

            return value
        }

    }


    private Object fixRefsList(List<Object> value) {

        value.collect { localValue ->

            if (localValue instanceof Map) {

                return this.fixRefsMap(localValue)

            } else if (localValue instanceof List) {

                return this.fixRefsList(localValue)

            } else {

                return localValue
            }

        }

    }


    @Override
    AWSCredentials getAWSCredentials() {
        return this.globalContext.getAWSCredentials()
    }


    @Override
    Process getProcess() {
        return this.globalContext.getProcess()
    }


    GlobalContext getGlobalContext() {
        return this.globalContext
    }


    static LocalContext getInstance(String contextId,
                                    Map<String, Object> localContextMap,
                                    GlobalContext globalContext) {

        LocalContext localContext = new LocalContext(
                contextId, localContextMap, globalContext
        )
        return localContext
    }


    @Override
    public String toString() {
        return "LocalContext{" +
                "contextMap=" + contextMap +
                '}';
    }
}
