package com.opkloud.kloudminions.context


abstract class AbstractContext implements Context {


	protected Map<String, Object> contextMap = null


	AbstractContext() {
		this.contextMap = new LinkedHashMap<String, Object>()
	}


	Map<String, Object> getContextMap() {
		return this.contextMap
	}


	void setContextMap(Map<String, Object> contextMap) {
		this.contextMap = contextMap
	}


	@Override
	 void add(String key, Object value) {

		// TODO Defensive Programming

		this.contextMap.put(key, value)
	}

	@Override
	void addAll(Map<String, Object> map) {

		// TODO Defensive Programming

		this.contextMap.putAll(map)
	}

	@Override
	void addToMap(String keyForItemInContextWithAMapValue, String keyForNewEntry, Object valueForNewEntry, boolean createIfNotExists) {

		// TODO Defensive Programming
		Map<String, Object> mapInContext = this.contextMap.get(keyForItemInContextWithAMapValue)

		if (mapInContext == null) {

			if (createIfNotExists == true) {
				mapInContext = new LinkedHashMap<String, Object>()
			} else {

				// TODO decide if we need an exception thrown here
				return
			}
		}

		if (mapInContext instanceof Map) {

			mapInContext.put(keyForNewEntry, valueForNewEntry)

		} else {

			// TODO decide if we need an exception thrown here
			return
		}


	} // END addToMap Method


	@Override
	void addToMap(String keyForItemInContextWithAMapValue, String keyForNewEntry, Object valueForNewEntry) {

		this.addToMap(keyForItemInContextWithAMapValue, keyForNewEntry, valueForNewEntry, true)
	}


	@Override
	void addToList(String keyForItemInContextWithAListValue, Object valueForNewListItem, boolean createIfNotExists) {

		// TODO Defensive Programming

		List<Object> ListInContext = this.contextMap.get(keyForItemInContextWithAListValue)

		if (ListInContext == null) {

			if (createIfNotExists == true) {
				ListInContext = new ArrayList<Object>()
			} else {

				// TODO decide if we need an exception thrown here
				return
			}
		}


		if (ListInContext instanceof List) {

			ListInContext.add(valueForNewListItem)

		} else {

			// TODO decide if we need an exception thrown here
			return
		}


	} // END addToList Method


	@Override
	void addToList(String keyForItemInContextWithAListValue, Object valueForNewListItem) {

		this.addToList(keyForItemInContextWithAListValue, valueForNewListItem, true)
	}


}
