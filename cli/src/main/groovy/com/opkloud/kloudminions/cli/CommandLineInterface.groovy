/**
 * 
 */
package com.opkloud.kloudminions.cli

import com.opkloud.kloudminions.exceptions.CommandLineException
import com.opkloud.kloudminions.cli.actions.CLIProcessorFactory
import com.opkloud.kloudminions.cli.options.RunProcessOptions
import com.opkloud.kloudminions.cli.processors.CLIProcessor
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * @author marco
 *
 */
class CommandLineInterface {

	static final int ERROR_EXIT_CODE = 1
	static final boolean PRINT_HELP_WITH_ERROR = true

	private static final Logger log = LoggerFactory.getLogger(CommandLineInterface.class)


	static CliBuilder cliBuilder = new CliBuilder(
				usage: 'minion [command] [options]',
				header: 'Options:', 
				stopAtNonOption : false)


	private static def onError(Throwable cause) {
		onError(cause, null, false)
	}


	private static void onError(Throwable cause, boolean printHelpWithError){

		printf("\n%s\n",cause.message)

		if (printHelpWithError){
			printHelp()
		}
		System.exit(ERROR_EXIT_CODE)
	}

	public static main(args) {
		
		try{
			OptionAccessor optionAccessor = parseArguments(args)

            if(optionAccessor.h){
                printHelp()
                return
            }

            CLIProcessor cliProcessor = CLIProcessorFactory.getInstance()

            cliProcessor.process(optionAccessor)
        }
        catch(CommandLineException e){
            onError(e, PRINT_HELP_WITH_ERROR)
        }
    }
	
    private static void printHelp(){
        cliBuilder.usage()
    }

	private static OptionAccessor parseArguments(String[] args){
        CommandLineOptions commandLineOptions = new RunProcessOptions()
        cliBuilder.setOptions(commandLineOptions.options)
        return cliBuilder.parse(args)
    }

    private static String minionActionFrom(def action){
        return action.toString().toUpperCase()
    }
	
} // END CommandLineInterface Class
