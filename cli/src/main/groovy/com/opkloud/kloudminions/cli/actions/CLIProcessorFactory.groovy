/**
 * 
 */
package com.opkloud.kloudminions.cli.actions

import com.opkloud.kloudminions.cli.processors.CLIProcessor
import com.opkloud.kloudminions.cli.processors.ProcessProcessor
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * @author Marco L. Jacobs
 *
 */
class CLIProcessorFactory {

	private static final Logger log = LoggerFactory.getLogger(CLIProcessorFactory.class)


	static CLIProcessor getInstance() {

		CLIProcessor processor  = new ProcessProcessor()

		return processor
	}

} // END CLIProcessorFactory Class
