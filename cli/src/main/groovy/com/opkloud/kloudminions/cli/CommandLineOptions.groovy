package com.opkloud.kloudminions.cli

import org.apache.commons.cli.Options;

interface CommandLineOptions {
	
	static final def REQUIRED_OPTION = true
	
	def Options getOptions()

}
