package com.opkloud.kloudminions.cli.processors

import com.amazonaws.auth.AWSCredentials
import com.opkloud.kloudminions.exceptions.CommandLineException
import com.opkloud.kloudminions.process.Process
import com.opkloud.kloudminions.auth.AWSCredentialsFactory
import com.opkloud.kloudminions.process.ProcessBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * @author Marco L. Jacobs
 *
 */
class ProcessProcessor implements CLIProcessor {

	private static String CURRENT_DIR_PATH  = "."
	private static String USER_HOME_DIR = System.getProperty("user.home")
	private static String CURRENT_DIR_CONFIG_PATH  = CURRENT_DIR_PATH + File.separator + "config"
	private static String DEFAULT_PROC_SPEC_FILE_NAME = "proc-spec.json"
	private static String DEFAULT_PROCESS_CONTEXT_FILE_NAME = "process-context.json"
	private static String DEFAULT_CREDENTIALS_FILE_NAME = "aws-credentials.json"
	private static String PROC_SPEC_CURRENT_DIR = CURRENT_DIR_PATH + File.separator + DEFAULT_PROC_SPEC_FILE_NAME
	private static String PROC_SPEC_CURRENT_CONFIG_DIR = CURRENT_DIR_CONFIG_PATH + File.separator + DEFAULT_PROC_SPEC_FILE_NAME
	private static String PROCESS_CONTEXT_CURRENT_DIR = CURRENT_DIR_PATH + File.separator + DEFAULT_PROCESS_CONTEXT_FILE_NAME
	private static String PROCESS_CONTEXT_CURRENT_CONFIG_DIR = CURRENT_DIR_CONFIG_PATH + File.separator + DEFAULT_PROCESS_CONTEXT_FILE_NAME
	private static String DEFAULT_CREDENTIALS_PATH = USER_HOME_DIR + File.separator + ".minions" + File.separator + DEFAULT_CREDENTIALS_FILE_NAME

	private static final Logger log = LoggerFactory.getLogger(ProcessProcessor.class)

	private OptionAccessor options

	@Override
	public void process(OptionAccessor options) {
		
		this.options = options

        ProcessBuilder processBuilder = new ProcessBuilder()
		
		String globalContextFilePath = this.getGlobalContextFilePath()
		File globalContextFile = new File(globalContextFilePath)
		String globalContextFileContents = globalContextFile.getText()

        processBuilder.setGlobalContextMap(globalContextFileContents)


		String credentialsFilePath = this.getCredentialsFilePath()
        AWSCredentials awsCredentials = AWSCredentialsFactory.getFromFile(credentialsFilePath)
		AWSCredentialsFactory.

        processBuilder.setAWSCredentials(awsCredentials)


		
		String processSpecFilePath = this.getProcessSpecFilePath()
		File processSpecFile = new File(processSpecFilePath)
		String processSpecFileContent = processSpecFile.getText()
		
		processBuilder.setProcessSpec(processSpecFileContent)


		
		Process process = processBuilder.build()
		process.execute()
		
	}


	private String getProcessSpecFilePath(){
		if(options.process){
			return options.process
		}
		else if(new File(PROC_SPEC_CURRENT_DIR).exists()){
			return PROC_SPEC_CURRENT_DIR
		}
		else if(new File(PROC_SPEC_CURRENT_CONFIG_DIR).exists()){
			return PROC_SPEC_CURRENT_CONFIG_DIR
		}
		else{
			throw new CommandLineException("No Process Specification file path was specified or found")
		}
	}


    private String getCredentialsFilePath(){
        if(options.credentials){
            return options.credentials
        }
		else if (new File(DEFAULT_CREDENTIALS_PATH).exists()){
			return DEFAULT_CREDENTIALS_PATH
		}
        else{
            throw new CommandLineException("No Credentials file path was specified or found")
        }
    }

    public String getGlobalContextFilePath(){
        if(options.context){
            return options.context
        }
		else if(new File(PROCESS_CONTEXT_CURRENT_DIR).exists()){
			return PROCESS_CONTEXT_CURRENT_DIR
		}
		else if(new File(PROCESS_CONTEXT_CURRENT_CONFIG_DIR).exists()){
			return PROCESS_CONTEXT_CURRENT_CONFIG_DIR
		}
        else{
            return null
        }
    }

} // END ProcessProcessor Class
