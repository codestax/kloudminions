/**
 * 
 */
package com.opkloud.kloudminions.cli.processors

/**
 * @author Marco L. Jacobs
 *
 */
interface CLIProcessor {

	public void process(OptionAccessor options)

} // END CLIProcessor Interface
