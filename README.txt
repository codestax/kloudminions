Some initial steps to take to get things working properly



Create file aws_credentials_pointer_file.json in the api directory

Example:
kloudminions/api/aws_credentials_pointer_file.json


This pointer file will eventually move to a more formal configuration system.  Until
then this file will allow us to maintain the security of our Amazon Access Keys by ensuring that
we are not tempted to, or inadvertently, check in our keys into the git repo.

Note: It's the absolute path from the root of the file system
Example: 
{
  "aws_credentials_file_path": "/Users/rashadmoore/.minions"
}

kloudminions cli makes a few assumptions if the following cli flags are not present:

**NOTE- underscores in the filenames have been replaces with "-" as the default filenames!**

-p (proc-spec.json) 
-c (aws-credentials)
-t (process-context.json)

If the -p flag is not provided, kloudminions will look for proc-spec.json in the current directory (./proc-spec.json) and also inside a config directory in the current directory (./config/proc-spec.json)

If the -c flag is not provided, kloudminions will look for process-context.json in the current directory (./process-context.json) and also inside a config directory in the current directory (./config/process-context.json)

If the -t flag is not provided, kloudminions will look for aws-credentials.json in "~/.minions/aws-credentials"



